﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapPiece : MonoBehaviour {

    public int CollisionDamage = 3;
    public bool canHit = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /*public void OnCollisionEnter(Collision collision)
    {
        // Handle ship to scrap collision here, theoritically we could just cast and see if it fits but rather make simple check
        if (collision.gameObject.GetComponent<Ship>().isShip) {
            Ship ship = collision.gameObject.GetComponent<Ship>();
            // Just generate some angle, not correct yet
            float contactAngle = Vector3.Angle(ship.GetComponent<Transform>().position, collision.contacts[0].point);            
            ship.RecieveAttack(CollisionDamage, contactAngle);
            Die();           
        }
    }*/

    public void OnTriggerEnter (Collider other){
        if (canHit && other.gameObject.GetComponent<ShipHitbox>()) {
            Ship ship = other.gameObject.GetComponent<ShipHitbox>().ship;
            // Just generate some angle, not correct yet
            float contactAngle = Vector3.Angle(ship.GetComponent<Transform>().position, transform.position);            
            ship.RecieveAttack(null ,CollisionDamage, contactAngle);
            Die();           
        }
    }

    // Capsulate death sequence here (particles, etc.)
    private void Die()
    {        
        Destroy(gameObject);
    }
}

