﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IonFieldCreator : MonoBehaviour, IObjectCreator
{

	public GameObject prefab;
	
	public int minField = 1;
	public int maxField = 4;
	

	public virtual bool WorkOnlyServer()
	{
		return false;
	}

	public virtual bool WorkOnMultiplay()
	{
		return false;
	}

	public virtual bool WorkOnOffline()
	{
		return true;
	}

	public void GenerateObject(MapGenerator mapGenerator)
	{
		for (int i = 0; i < Random.Range(minField, maxField + 1); i++)
		{
			foreach (var section in mapGenerator.SelectPairSections())
			{
				CreateField( section.bounds);
			}

		}
	}
	
	public virtual void CreateField( Bounds fieldAABB)
	{
		Vector3 pos = fieldAABB.center;
		pos.z = 0;
		Instantiate(prefab, pos, Quaternion.identity, this.transform);
	}
}
