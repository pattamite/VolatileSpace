﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkAsteroidCreator : AsteroidCreator {


	public override bool WorkOnMultiplay()
	{
		return true;
	}
	
	public override bool WorkOnlyServer()
	{
		return true;
	}

	public override bool WorkOnOffline()
	{
		return false;
	}
	
	public override void CreateAsteroid(Vector3 position, Vector3 direction)
	{
		GameObject go = Instantiate(prefab, position, Quaternion.identity, this.transform);
		Asteroid asteroid = go.GetComponent<Asteroid>();
		asteroid.direction = direction;
		NetworkServer.Spawn(go);
		asteroid.isAllowWorking = true;
	}
}
