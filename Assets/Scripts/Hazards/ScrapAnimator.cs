﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapAnimator : MonoBehaviour {

    private Vector3 axels;

	// Use this for initialization
	void Start () {
        axels = Random.insideUnitSphere;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(axels);
	}
}
