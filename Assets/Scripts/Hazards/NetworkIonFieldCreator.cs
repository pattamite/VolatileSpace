﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkIonFieldCreator : IonFieldCreator {

    public override bool WorkOnMultiplay()
    {
        return true;
    }
	
    public override bool WorkOnlyServer()
    {
        return true;
    }

    public override bool WorkOnOffline()
    {
        return false;
    }
    
    public override void CreateField( Bounds fieldAABB)
    {
        Vector3 pos = fieldAABB.center;
        pos.z = 0;
        GameObject go = Instantiate(prefab, pos, Quaternion.identity, this.transform);
        NetworkServer.Spawn(go);
    }
}
