﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Asteroid : MonoBehaviour
{
    [System.Serializable]
    public struct AstroidSpawnInfo {
        public Vector3 relativePosition;
        public Vector3 direction;
    }
    
    public bool isAllowWorking = false;
    
    [Header("Posittion Bounds")]
    public float boundTop;
    public float boundBottom;
    public float boundLeft;
    public float boundRight;

    [Header("Speed")]
    public Vector3 direction;
    public float baseSpeed;
    public float randomSpeedRange;
    private float speed;
    
    [Header("HP & Damage")]
    public float maxHp;
    private float currentHp;
    public float attackDamage;
    public bool destroyWhenHitShip = false;

    [Header("Spawn Info")]
    public GameObject asteroidToSpawn;
    public List<AstroidSpawnInfo> spawnInfoList = new List<AstroidSpawnInfo>();

    //flags
    private bool isMoving = false;

    void OnValidate(){
        if(boundTop < boundBottom){
            float temp = boundTop;
            boundTop = boundBottom;
            boundBottom = temp;
        }

        if(boundRight < boundLeft){
            float temp = boundLeft;
            boundLeft = boundRight;
            boundRight = temp;
        }
    }

    void Start()
    {
        currentHp = maxHp;
        speed = baseSpeed + Random.Range(-randomSpeedRange, randomSpeedRange);
        direction = direction.normalized;

        // If this asteroid is created in Excute state, add thrust immediately.
        if(GameStateController.currentGameState == GameStateController.GameStates.Execute){
            isMoving = true;
        }

        //Subscribe GameStateController.OnStateActivate event.
        GameStateController.OnStateActivate += OnStateActivate;

        AsteroidCreator.astroidCount += 1;
    }


    // Update is called once per frame
    void Update(){

    }

    void FixedUpdate(){
        if (!isAllowWorking)
            return;
        Movement();
    }

    private void Movement(){
        if(isMoving){
            transform.position += speed * direction * Time.fixedDeltaTime;
        }

        CheckOutOfBound();
    }

    public void CheckOutOfBound(){
        Vector3 newPos = transform.position;
        if (transform.position.y > boundTop || transform.position.y < boundBottom ||
            transform.position.x > boundRight || transform.position.x < boundLeft){
            Dead(false);
        }

    }

    public void SetSpawnInfo(AstroidSpawnInfo info, Vector3 parentPosition){
        transform.position = parentPosition + info.relativePosition;
        direction = info.direction.normalized;
    }

    public void RecieveAttack(float damage){
        currentHp = Mathf.Clamp(currentHp - damage, 0, maxHp);

        if(currentHp <= 0){
            Dead(true);
        }
    }

    private void Dead(bool spawnAstroid){
        if (asteroidToSpawn && spawnInfoList.Count > 0 && spawnAstroid){
            foreach(AstroidSpawnInfo info in spawnInfoList){
                Asteroid newAstroid = Instantiate(asteroidToSpawn).GetComponent<Asteroid>();
                newAstroid.SetSpawnInfo(info, transform.position);
                if (NetworkController.instance != null && NetworkController.instance.isServer)
                {
                    Debug.Log("Spawn Asteroid On Server");
                    NetworkServer.Spawn(newAstroid.gameObject);
                }
                newAstroid.isAllowWorking = true;
            }
            AudioHandler.CreateSound("SFX_AsteroidSplit", transform.position);
        }
        
        Destroy(gameObject);
    }


    private void OnDestroy(){
        //Unsubscribe GameStateController.OnStateActivate event to prevent memory leaked.
        GameStateController.OnStateActivate -= OnStateActivate;
        AsteroidCreator.astroidCount -= 1;
    }

    private void OnTriggerEnter(Collider other){
        if (!isAllowWorking)
            return;
        
        ShipHitbox shipHitbox = other.GetComponent<ShipHitbox>();
		if(shipHitbox && shipHitbox.ship){
			float contactAngle = Vector3.Angle(shipHitbox.ship.GetComponent<Transform>().position, transform.position);            
            shipHitbox.ship.RecieveAttack(null, attackDamage, contactAngle);

            if(destroyWhenHitShip){
                Dead(true);
            }
		}
    }

    private void OnStateActivate(GameStateController.GameStates state){
        switch(state)
        {
            // When Excute state start, move asteroid.
            case GameStateController.GameStates.Execute :
                isMoving = true;
                break;
            // When other state start, stop asteroid.
            default :
                isMoving = false;
                break;
        }
    }
}
    
    



