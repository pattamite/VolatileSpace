﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidCreator : MonoBehaviour , IObjectCreator
{

	public Collider targetDirectionArea;
	
	public GameObject prefab;

	public MapGenerator allowableMapGenerator;

	public static int astroidCount = 0;
	
	public virtual bool WorkOnlyServer()
	{
		return false;
	}

	public virtual bool WorkOnMultiplay()
	{
		return false;
	}

	public virtual bool WorkOnOffline()
	{
		return true;
	}

	public void GenerateObject(MapGenerator mapGenerator)
	{
		if (mapGenerator != allowableMapGenerator)
			return;
		
		Vector3 pos = mapGenerator.SelectSection().bounds.center;

		Vector3 extends = targetDirectionArea.bounds.extents;
		Vector3 targetPos = new Vector3();
		targetPos.x = Random.Range(-extends.x, extends.x);
		targetPos.y = Random.Range(-extends.y, extends.y);
		targetPos = targetDirectionArea.bounds.center + targetPos;
		Vector3 dir = targetPos - pos;
		
		CreateAsteroid(pos, dir.normalized);
	}

	public virtual void CreateAsteroid(Vector3 position, Vector3 direction)
	{
		GameObject go = Instantiate(prefab, position, Quaternion.identity, this.transform);
		Asteroid asteroid = go.GetComponent<Asteroid>();
		asteroid.direction = direction;
		asteroid.isAllowWorking = true;
	}
}
