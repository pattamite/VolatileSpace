﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IonField : MonoBehaviour {
	[Header("Parameters")]
	[Range(0f, 1f)] public float energyMultiplayer = 0.5f;
	[Range(0f, 200f)] public float damagePerTurn = 20f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other){
        ShipHitbox shipHitbox = other.GetComponent<ShipHitbox>();
		if(shipHitbox && shipHitbox.ship){
			shipHitbox.ship.EnterIonField(this);
		}
    }

	private void OnTriggerExit(Collider other){
        ShipHitbox shipHitbox = other.GetComponent<ShipHitbox>();
		if(shipHitbox && shipHitbox.ship){
			shipHitbox.ship.ExitIonField(this);
		}
    }

	public static void CheckIonField(Ship ship, List<IonField> ionFieldList){
			IonField field = GetMinMultiplyerField(ionFieldList);

			IonField.EffectDeactivate(ship);
			if(field){
				IonField.EffectActivate(ship, field);
			}
	}

	public static void CheckEndTurnIonField(Ship ship, List<IonField> ionFieldList){
			IonField field = GetMaxDamageField(ionFieldList);

			if(field){
				IonField.EndturnEffect(ship, field);
			}
	}

	private static void EffectActivate(Ship ship, IonField field){
		ship.ForceHide();
		ship.SetHealthBarToIonFieldColor();
		ship.ApplyEnergyMultiplyer(field.energyMultiplayer);
	}

	private static void EffectDeactivate(Ship ship){
		ship.StopForceHide();
		ship.SetHealthBarToNormalColor();
		ship.ResetEnergyMultiplyer();
	}

	private static void EndturnEffect(Ship ship, IonField field){
		ship.RecieveAttack(null, field.damagePerTurn, 0, true);
	}

	private static IonField GetMinMultiplyerField(List<IonField> ionFieldList){
		if(ionFieldList.Count <= 0) return null;

		float minMultiplyer = float.MaxValue;
		int fieldIndex = 0;
		for(int i = 0 ; i < ionFieldList.Count ; i++){
			if(ionFieldList[i].energyMultiplayer < minMultiplyer){
				minMultiplyer = ionFieldList[i].energyMultiplayer;
				fieldIndex = i;
			}
		}

		return ionFieldList[fieldIndex];
	}

	private static IonField GetMaxDamageField(List<IonField> ionFieldList){
		if(ionFieldList.Count <= 0) return null;

		float maxDamage = 0;
		int fieldIndex = 0;
		for(int i = 0 ; i < ionFieldList.Count ; i++){
			if(ionFieldList[i].damagePerTurn > maxDamage){
				maxDamage = ionFieldList[i].damagePerTurn;
				fieldIndex = i;
			}
		}

		return ionFieldList[fieldIndex];
	}
}
