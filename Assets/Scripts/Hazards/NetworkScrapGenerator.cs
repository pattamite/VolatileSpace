﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkScrapGenerator : NetworkBehaviour
{

	public NetworkIdentity[] PiecePrefabs;

	public GameObject CreateScrap(Vector3 pos)
	{
		if (! isServer)
			return null;
		
		Debug.Log("Create NetworkScrap");
		NetworkIdentity go = PiecePrefabs[Random.Range(0, PiecePrefabs.Length - 1)];
            
		GameObject gameObject = Instantiate(go, pos, Quaternion.identity, transform).gameObject;
		NetworkServer.Spawn(gameObject);
		gameObject.GetComponent<ScrapPiece>().canHit = true;
		return gameObject;
	}
	
}
