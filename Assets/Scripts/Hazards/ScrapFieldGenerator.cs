﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapFieldGenerator : MonoBehaviour {

    public float minSize = 0.25f;
    public Bounds PlayArea;

    public List<GameObject> PiecePrefabs = new List<GameObject>();

    // Add any boundary objects that block spawning scrap objects (spawnzones, other hazards, etc).
    public List<GameObject> ExclusionZones = new List<GameObject>();

    private List<Bounds> fields = new List<Bounds>();
    public NetworkScrapGenerator extendGenerator;
   

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (Bounds AABB in fields)
            Gizmos.DrawWireCube(AABB.center, AABB.size);
    }

    public void CreateField(int pieces)
    {

        Debug.Log("Creating scrap field with pieces: " + pieces.ToString());
        // Generate suggested point where to place the field
        Vector3 fieldPosition = GetPointInsidePlayArea();      
        
        // Setup AABB containing enough space to hold n pieces, inflate Z to be 1 regardless
        float minDist = minSize * pieces;
        float size = Random.Range(minDist, minDist + 1.0f);
        Vector3 extends = new Vector3(size, size, 1.0f);
        Bounds fieldAABB = new Bounds(fieldPosition, extends);
        Debug.Log(fieldAABB);


        // Check for intersecting exclusion zones, this implementation is simple, heavy and does not guarantee success
        int maxAttempts = 100;
        int tries = 0;
        bool safePoint = false;
        while (!safePoint)
        {
            foreach (GameObject obj in ExclusionZones)
            {
                if (obj.GetComponent<Collider>().bounds.Intersects(fieldAABB))
                {
                    safePoint = false;                    
                    fieldPosition = GetPointInsidePlayArea();
                    fieldAABB.center = fieldPosition;
                    break;
                }
                safePoint = true;
            }
            tries++;
            if(tries>maxAttempts)
            {
                Debug.Log("Cannot find point to create scrapfield");
                break;
            }

        }

        Debug.Log("Field position " + fieldPosition);

        // Once we've found safe point for our scrap field to exist, generate the required amount of pieces inside, currently doesnt care if pieces clip eachother
        for (int i = 0; i != pieces; i++)
        {
            float xpos = Random.Range(fieldAABB.min.x, fieldAABB.max.x);
            float ypos = Random.Range(fieldAABB.min.y, fieldAABB.max.y);
            Vector3 piecePosition = new Vector3(xpos, ypos, 0.0f);
            
            // Parent the scrap objects to fieldmanager to make them bit more grouped
            CreateScrap(piecePosition);

            /*
            for (int i = 0; i != pieces; i++) {
                GameObject go = PiecePrefabs[Random.Range(0, PiecePrefabs.Count - 1)];
                Vector3 piecePos = new Vector3();
                bool safePoint = false;

                // Simple way to find point safe for spawning the piece, needs refining (such as max attempts, possible space partitioning)
                // works ok as long there are not too many zones and pieces to find
                Vector3 safeDist = go.GetComponentInChildren<Renderer>().bounds.extents;


                while (!safePoint) {
                    piecePos = fieldPosition + Random.insideUnitSphere * size;
                    piecePos.z = 0.0f;                
                    foreach (GameObject obj in ExclusionZones) {
                        if (obj.GetComponent<Collider>().bounds.Contains(piecePos+safeDist) || obj.GetComponent<Collider>().bounds.Contains(piecePos - safeDist)) {
                            safePoint = false;
                            Debug.Log("Position inside " + piecePos);
                            break;
                        }
                        safePoint = true;
                    }                
                }         
                if(safePoint)
                    Instantiate(go, piecePos, Quaternion.identity, transform);

            }*/
        }
        fields.Add(fieldAABB);
    }

    private GameObject CreateScrap(Vector3 pos)
    {
        if (!GameController.isOfflineModeActivate)
        {
            if (extendGenerator != null)
            {
                return extendGenerator.CreateScrap(pos);
            }
        }
        else
        {
            Debug.Log("Create Default Scrap");
            
            GameObject go = PiecePrefabs[Random.Range(0, PiecePrefabs.Count - 1)];
            
            // Parent the scrap objects to fieldmanager to make them bit more grouped
            return Instantiate(go, pos, Quaternion.identity, transform);
        }
        return null;
    }

    private Vector3 GetPointInsidePlayArea() {
        float fieldX = Random.Range(PlayArea.min.x, PlayArea.max.x);
        float fieldY = Random.Range(PlayArea.min.y, PlayArea.max.y);
        return new Vector3(fieldX, fieldY, PlayArea.center.z);
    }

}
