﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkScrapFieldCreator : ScrapFieldCreator {


	public override bool WorkOnMultiplay()
	{
		return true;
	}
	
	public override bool WorkOnlyServer()
	{
		return true;
	}

	public override bool WorkOnOffline()
	{
		return false;
	}
	
	
	public override GameObject CreateScrap(Vector3 pos)
	{	
		Debug.Log("Create Network Scrap");
		GameObject go = piecePrefabs[Random.Range(0, piecePrefabs.Count)];
            
		GameObject gameObject = Instantiate(go, pos, Quaternion.identity, transform).gameObject;
		NetworkServer.Spawn(gameObject);
		gameObject.GetComponent<ScrapPiece>().canHit = true;
		return gameObject;
	}
}
