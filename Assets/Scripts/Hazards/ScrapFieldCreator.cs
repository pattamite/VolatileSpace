﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapFieldCreator : MonoBehaviour, IObjectCreator
{

	public float minSize = 0.25f;
	public int minField = 1;
	public int maxField = 4;

	public int minPiecesPerField = 5;
	public int maxPiecesPerField = 5;
	
	public List<GameObject> piecePrefabs;

	public virtual bool WorkOnlyServer()
	{
		return false;
	}

	public virtual bool WorkOnMultiplay()
	{
		return false;
	}

	public virtual bool WorkOnOffline()
	{
		return true;
	}

	public virtual void GenerateObject(MapGenerator mapGenerator)
	{
		for (int i = 0; i < Random.Range(minField, maxField + 1); i++)
		{
			int number = Random.Range(minPiecesPerField, maxPiecesPerField);
			foreach (var section in mapGenerator.SelectPairSections())
			{
				CreateField(number, section.bounds);
			}

		}
	}

	public virtual void CreateField(int pieces, Bounds fieldAABB)
	{

		Debug.Log("Creating scrap field with pieces: " + pieces.ToString());
		// Generate suggested point where to place the field    

		// Once we've found safe point for our scrap field to exist, generate the required amount of pieces inside, currently doesnt care if pieces clip eachother
		for (int i = 0; i != pieces; i++)
		{
			float xpos = Random.Range(fieldAABB.min.x, fieldAABB.max.x);
			float ypos = Random.Range(fieldAABB.min.y, fieldAABB.max.y);
			Vector3 piecePosition = new Vector3(xpos, ypos, 0.0f);

			// Parent the scrap objects to fieldmanager to make them bit more grouped
			CreateScrap(piecePosition);
		}
	}

	public virtual GameObject CreateScrap(Vector3 pos)
	{

		Debug.Log("Create Offline Scrap");

		GameObject go = piecePrefabs[Random.Range(0, piecePrefabs.Count)];

		// Parent the scrap objects to fieldmanager to make them bit more grouped
		return Instantiate(go, pos, Quaternion.identity, transform);
	}
}
