﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEngineSoundController : VolumeParameterController {

  public float planPhaseMultiplier = 0.3f;
  private bool isStarted = false;

  void Start() {
    GameStateController.OnStateActivate += WaitExecute;
    GameStateController.OnStateActivate += TriggerPhase;
    VolumeController.AudioSystems.Add(this.gameObject);
  }

  void WaitExecute(GameStateController.GameStates receiveState) {
    if (receiveState == GameStateController.GameStates.Execute) {
      if (isStarted == false) {
        this.gameObject.GetComponent<AudioLoopingController>().AudioPlay();
      }
      isStarted = true;
      GameStateController.OnStateActivate -= WaitExecute;
    }
  }

  void OnDestroy() {
    GameStateController.OnStateActivate -= WaitExecute;
    GameStateController.OnStateActivate -= TriggerPhase;
    VolumeController.AudioSystems.Remove(this.gameObject);
  }

  void TriggerPhase(GameStateController.GameStates receiveState) {
    RefreshVolume();
  }

  void RefreshVolume() {
    this.gameObject.GetComponent<AudioLoopingController>().RefreshVolume();
  }

  public override float getMultiplier() {
    bool shouldPlaySound = GameController.IsPlayerId(this.transform.parent.GetComponent<Ship>().shipOwnerId) || this.transform.parent.GetComponent<Ship>().IsSeen();
    if (shouldPlaySound) {
      float multiplier = ((float)this.transform.parent.GetComponent<Ship>().GetCurrentEnergy(ShipEnergy.EnergyType.Maneuver) / this.transform.parent.GetComponent<Ship>().GetMaxEnergy(ShipEnergy.EnergyType.Maneuver) * 2.0f);
      if (GameStateController.currentGameState != GameStateController.GameStates.Execute) {
        multiplier = multiplier * planPhaseMultiplier;
      }
      return multiplier;
    }
    else {
      return 0.0f;
    }
  }

}
