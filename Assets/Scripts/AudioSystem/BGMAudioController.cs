﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMAudioController : AudioController {

  public AudioReverbZone PlanningReverb;

  void Start () {
    GameStateController.OnStateActivate += RefreshEffect;
    VolumeController.AudioSystems.Add(this.gameObject);
    RefreshVolume();
  }

  void Update () {

  }

  void OnDestroy(){
    GameStateController.OnStateActivate -= RefreshEffect;
    VolumeController.AudioSystems.Remove(this.gameObject);
  }

  void RefreshEffect(GameStateController.GameStates receiveState) {
    if (receiveState == GameStateController.GameStates.Execute) {
      //AudioHandler.CreateSound("UI_PhaseChange");
      //PlanningReverb.gameObject.SetActive(false);
    }
    else if (receiveState == GameStateController.GameStates.Planning) {
      AudioHandler.CreateSound("UI_PhaseChange");
      //PlanningReverb.gameObject.SetActive(true);
    }
  }
}
