﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLoopingController : MonoBehaviour {

  public AudioClip audioIntro;
  public AudioClip audioLoop;
  public AudioClip audioOutro;

  private AudioSource [] sources;

  private bool isEnding = false;

  public bool playOnStart = false;
  public float spatialBlend = 0.0f;

  private float getVolume() {
    if (this.gameObject.GetComponent<VolumeParameterController>() != null) {
      return this.gameObject.GetComponent<VolumeParameterController>().getVolume();
    }
    else {
      return VolumeController.GetVolumeByTag(gameObject.tag);
    }
  }

  void Start () {
    sources = new AudioSource[3];
    sources [0] = gameObject.AddComponent<AudioSource>();
    sources [1] = gameObject.AddComponent<AudioSource>();
    sources [2] = gameObject.AddComponent<AudioSource>();

    sources [0] .clip = audioIntro;
    sources [1] .clip = audioLoop;
    sources [2] .clip = audioOutro;
    if (audioOutro == null) {
      sources [2] .clip = audioLoop;
    }

    sources [0] .loop = false;
    sources [1] .loop = true;
    sources [2] .loop = false;

    sources [0] .volume = getVolume();
    sources [1] .volume = getVolume();
    sources [2] .volume = 0.0f;

    sources [0] .spatialBlend = spatialBlend;
    sources [1] .spatialBlend = spatialBlend;
    sources [2] .spatialBlend = spatialBlend;

    if (playOnStart == true) {
      AudioPlay();
    }

    VolumeController.AudioSystems.Add(this.gameObject);
  }

  void OnDestroy () {
    VolumeController.AudioSystems.Remove(this.gameObject);
  }

  public void AudioPlay() {
    if (sources [0] == null) {
      Debug.Log("Audio not loaded");
      return;
    }
    Debug.Log("Start Playing @ " + sources [0] .volume );
    isEnding = false;
    sources [0] .Stop();
    sources [1] .Stop();
    sources [2] .Stop();
    if (sources [0] .clip != null) {
      sources [0] .Play();
    }
    if (sources [1] .clip != null) {
      sources [1] .PlayScheduled(AudioSettings.dspTime + audioIntro.length);
    }
  }

  public void RefreshVolume() {
    if (isEnding == false) {
      sources [0] .volume = getVolume();
      sources [1] .volume = getVolume();
      sources [2] .volume = 0.0f;
    }
    // Below only run after TriggerEnd()
    else if (sources [2] .isPlaying == true) {
      sources [0] .volume = 0.0f;
      sources [1] .volume = 0.0f;
      sources [2] .volume = getVolume();
    }
    else if (sources [0] .isPlaying == false) {
      sources [0] .volume = 0.0f;
      sources [2] .volume = getVolume();
      sources [2] .PlayScheduled(AudioSettings.dspTime + (sources [1] .clip.length - sources [1] .time));
      sources [1] .volume = 0.0f;
    }
    else { // Intro is not finished yet, Outro is waiting
      sources [0] .volume = getVolume();
      sources [2] .volume = getVolume();
      sources [2] .PlayScheduled(AudioSettings.dspTime + (sources [0] .clip.length - sources [0] .time));
      sources [1] .volume = 0.0f;
    }
  }

  public void TriggerEnd() {
    isEnding = true;
    RefreshVolume();
  }
}
