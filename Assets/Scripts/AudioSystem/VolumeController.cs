﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeController : MonoBehaviour {
  public enum VolumeType {BGM ,SFX, UI};
  public static List<GameObject> AudioSystems = new List<GameObject>();

  // TODO : Fix the static linking
  public static string configHandlerBGM = "a_BGM";
  public static string configHandlerSFX = "a_SFX";
  public static string configHandlerUI = "a_UI";

  // Volume values are range 0.0f to 1.0f
  public static float GetVolume(VolumeType AudioType) {
    switch (AudioType) {
      case VolumeType.BGM :
        return ScaleVolume(PlayerPrefs.GetFloat(configHandlerBGM, 1.0f));
      case VolumeType.SFX :
        return ScaleVolume(PlayerPrefs.GetFloat(configHandlerSFX, 1.0f));
      case VolumeType.UI :
        return ScaleVolume(PlayerPrefs.GetFloat(configHandlerUI, 1.0f));
      default :
        return 0.0f;
    }
  }

  public static float GetVolumeByTag(string tag) {
    if (tag == "SoundBGM") {
      return(GetVolume(VolumeType.BGM));
    }
    else if (tag == "SoundSFX") {
      return(GetVolume(VolumeType.SFX));
    }
    else if (tag == "SoundUI") {
      return(GetVolume(VolumeType.UI));
    }
    return 0.0f;
  }

  public static float ScaleVolume(float percentage) {
    float result = Mathf.Pow(percentage, 2) / 2.0f;
    //Debug.Log("Scaled to = " + result);
    return result;
  }


  public static void RefreshVolume() {
    foreach (GameObject audioSystem in AudioSystems) {
      if (audioSystem.GetComponent<AudioController>()) {
        audioSystem.GetComponent<AudioController>().RefreshVolume();
      }
      else if (audioSystem.GetComponent<AudioLoopingController>()) {
        audioSystem.GetComponent<AudioLoopingController>().RefreshVolume();
      }
    }
  }
}
