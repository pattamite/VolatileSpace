﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour {

  [System.Serializable]
  public struct AudioDef {
    public AudioClip audioFile;
    public AudioTags tag;
  }

  [System.Serializable]
  public struct AudioDict {
    public string audioKey;
    public AudioDef audioTarget;
  } ;

  public List<AudioDict> AudioDictionary = new List <AudioDict>();
  public enum AudioTags {SoundBGM, SoundSFX, SoundUI, SoundEvent};

  public static Dictionary<string, AudioDef> ActualDict = new Dictionary <string, AudioDef>();

  private static List <AudioSource> playingList = new List <AudioSource>();

  public static int TooMany = 40;

  void Start () {
    PopulateDict();
    VolumeController.AudioSystems.Add(this.gameObject);
    GameStateController.OnStateActivate += CheckGarbage;
  }

  public static void CheckGarbage(GameStateController.GameStates receiveState) {
    if (receiveState == GameStateController.GameStates.Execute ||
      receiveState == GameStateController.GameStates.Ending) {
      GarbageCollect();
    }
  }

  public static void CheckTooManyAudio() {
    if (playingList.Count >= TooMany) {
      GarbageCollect();
    }
  }

  public static void GarbageCollect() {
    foreach (AudioSource audio in playingList.ToArray()) {
      if (audio && ! audio.isPlaying) {
        playingList.Remove(audio);
        Destroy(audio.gameObject);
      }
    }
  }

  void OnDestroy () {
    GameStateController.OnStateActivate -= CheckGarbage;
    VolumeController.AudioSystems.Remove(this.gameObject);
  }

  void PopulateDict() {
    foreach (AudioDict value in AudioDictionary) {
      if (value.audioKey != null) {
        ActualDict.Add(value.audioKey, value.audioTarget);
      }
    }
  }

  public void CreateSoundInstance(string key){
    CreateSound(key, null);
  }

  /// <summary>
  ///   Create and play the sound from a key, and set the parent if needed
  /// </summary>
  public static void CreateSound(string key, GameObject parent = null) {
    if(ActualDict.ContainsKey(key)) {
      AudioDef result;
      ActualDict.TryGetValue(key, out result);
      if (result.audioFile == null) {
        Debug.Log("AudioClip is missing for " + key);
        return;
      }
      Vector3 offset = new Vector3(0.0f, 0.0f, 0.0f);
      PlaySound(result, parent, offset);
    }
    else {
      Debug.Log("AudioEntry is missing for " + key);
    }
  }

  /// <summary>
  ///   Create and play the sound from a key, and set position of the sound.
  /// </summary>
  public static void CreateSound(string key, Vector3 position) {
    if(ActualDict.ContainsKey(key)) {
      Debug.Log("Audio = Playing " + key);
      AudioDef result;
      ActualDict.TryGetValue(key, out result);
      if (result.audioFile == null) {
        Debug.Log("Audio = AudioClip is missing for " + key);
        return;
      }
      PlaySound(result, null, position);
    }
    else {
      Debug.Log("Audio = AudioEntry is missing for " + key);
    }
  }

  public static void ProcessEvent(string key, GameObject parent, Vector3 offset) {
    return ;
  }

  /// <summary>
  ///   Create and play the sound from a key, and set parent and relative offset from the parent
  /// </summary>
  public static void CreateSound(string key, GameObject parent, Vector3 offset) {
    if(ActualDict.ContainsKey(key)) {
      Debug.Log("Audio = Playing " + key);
      AudioDef result;
      ActualDict.TryGetValue(key, out result);
      if (result.tag == AudioTags.SoundEvent) {
        ProcessEvent(key, parent, offset);
        return ;
      }
      if (result.audioFile == null) {
        Debug.Log("Audio = AudioClip is missing for " + key);
        return;
      }
      PlaySound(result, parent, offset);
    }
    else {
      Debug.Log("Audio = AudioEntry is missing for " + key);
    }
  }

  private static void PlaySound(AudioDef audio, GameObject parent, Vector3 offset) {
    AudioSource created = (new GameObject()).AddComponent<AudioSource>();
    created.clip = audio.audioFile;
    switch (audio.tag) {
      case AudioTags.SoundBGM :
        created.volume = VolumeController.GetVolume(VolumeController.VolumeType.BGM);
        created.gameObject.tag = "SoundBGM";
        break;
      case AudioTags.SoundSFX :
        created.volume = VolumeController.GetVolume(VolumeController.VolumeType.SFX);
        created.gameObject.tag = "SoundSFX";
        break;
      case AudioTags.SoundUI :
        created.volume = VolumeController.GetVolume(VolumeController.VolumeType.UI);
        created.gameObject.tag = "SoundUI";
        break;
    }
    if (parent != null) {
      created.transform.SetParent(parent.transform);
      created.transform.localPosition = offset;
      created.spatialBlend = 1.0f;
    }

    created.Play();
    playingList.Add(created);
    CheckTooManyAudio();
  }

  void Update () {
  }

  void RefreshVolume() {
    // ** -----
    foreach (AudioSource audio in playingList) {
      if (audio == null) {
        continue;
      }
      else {
        audio.volume = VolumeController.GetVolumeByTag(audio.gameObject.tag);
      }
    }
    // ** -----
    foreach (Transform childAudio in this.transform) {
      AudioSource audio = childAudio.gameObject.GetComponent<AudioSource>();
      if (audio == null) {
        continue;
      }
      else {
        audio.volume = VolumeController.GetVolumeByTag(audio.gameObject.tag);
      }
    }
  }
}
