﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioController : AudioController {

  // One-shot - for instantiating
  public AudioSource UISelect;
  public AudioSource UIDragStart;
  public AudioSource UIDragEnd;

  // Looping - Only single instance
  public AudioSource UIDragContinue;


  // Use this for initialization
  void Start () {
    PlayerController.OnShipSelected += SoundSelect;
    PlayerController.OnBeginCommandDrag += SoundDragStart;
    PlayerController.OnEndCommandDrag += SoundDragEnd;
    VolumeController.AudioSystems.Add(this.gameObject);
    RefreshVolume();
  }

  private void OnDestroy(){
    PlayerController.OnShipSelected -= SoundSelect;
    PlayerController.OnBeginCommandDrag -= SoundDragStart;
    PlayerController.OnEndCommandDrag -= SoundDragEnd;

    VolumeController.AudioSystems.Remove(this.gameObject);
  }

  void SoundSelect (Ship ship = null) {
    Debug.Log("Event = Select");
    // Play "Select" Sound
    CreateSound(UISelect, defaultTag);
  }

  void SoundDragStart (Ship ship = null) {
    Debug.Log("Event = Drag Start");
    // Play "DragStart" Sound
    CreateSound(UIDragStart, defaultTag);
    // Play "DragContinue" Sound
    Transform ownSound = this.transform.Find("UIDragContinue");
    if (ownSound) {
      ownSound.GetComponent<AudioSource>().Play();
    }
//    UIDragContinue.Play();
  }

  void SoundDragEnd (Ship ship = null) {
    Debug.Log("Event = Drag End");
    // Stop "DragContinue" Sound
    Transform ownSound = this.transform.Find("UIDragContinue");
    if (ownSound) {
      ownSound.GetComponent<AudioSource>().Stop();
    }
//    UIDragContinue.Stop();
    // Play "DragEnd" Sound
    CreateSound(UIDragEnd, defaultTag);
  }
}
