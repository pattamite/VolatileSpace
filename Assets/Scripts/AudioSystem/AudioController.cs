﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

  public string defaultTag = "";

  private static List<AudioSource> playingList = new List<AudioSource> ();

  public void RefreshVolume() {
    // ** -----
    foreach (AudioSource audio in playingList) {
      if (audio == null) {
        continue;
      }
      else if (audio.gameObject.tag == "SoundBGM") {
        audio.volume = VolumeController.GetVolume(VolumeController.VolumeType.BGM);
      }
      else if (audio.gameObject.tag == "SoundSFX") {
        audio.volume = VolumeController.GetVolume(VolumeController.VolumeType.SFX);
      }
      else if (audio.gameObject.tag == "SoundUI") {
        audio.volume = VolumeController.GetVolume(VolumeController.VolumeType.UI);
      }
    }
    // ** -----
    foreach (Transform childAudio in this.transform) {
      AudioSource audio = childAudio.gameObject.GetComponent<AudioSource>();
      if (audio == null) {
        continue;
      }
      else {
        audio.volume = GetVolumeByTag(audio.gameObject.tag);
      }
    }
  }

  public static float GetVolumeByTag(string tag) {
    if (tag == "SoundBGM") {
      return(VolumeController.GetVolume(VolumeController.VolumeType.BGM));
    }
    else if (tag == "SoundSFX") {
      return(VolumeController.GetVolume(VolumeController.VolumeType.SFX));
    }
    else if (tag == "SoundUI") {
      return(VolumeController.GetVolume(VolumeController.VolumeType.UI));
    }
    return 0.0f;
  }

  public static void GarbageCollect() {
    // Check if AudioSource finish playing
    foreach (AudioSource audio in playingList.ToArray()) {
      if (audio &&! audio.isPlaying) {
        playingList.Remove(audio);
        Destroy(audio.gameObject);
      }
    }
  }

  public AudioSource CreateSound(AudioSource AudioTemplate, string AudioType, GameObject AudioParent = null) {
    AudioSource createdSound = Instantiate<AudioSource>(AudioTemplate);
    createdSound.gameObject.tag = AudioType;
    if (createdSound.gameObject.tag == "SoundBGM") {
      createdSound.volume = VolumeController.GetVolume(VolumeController.VolumeType.BGM);
    }
    else if (createdSound.gameObject.tag == "SoundSFX") {
      createdSound.volume = VolumeController.GetVolume(VolumeController.VolumeType.SFX);
    }
    else if (createdSound.gameObject.tag == "SoundUI") {
      createdSound.volume = VolumeController.GetVolume(VolumeController.VolumeType.UI);
    }
    if (AudioParent != null) {
      createdSound.transform.SetParent(AudioParent.transform);
      // TODO : Add offset param
      createdSound.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
    }
    createdSound.Play();
    playingList.Add(createdSound);
    return createdSound;
  }

  void Start () {

  }

  void Update () {
    // TODO : Move to garbage collection routine
    GarbageCollect();
  }

}
