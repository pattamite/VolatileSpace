﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VolumeParameterController : MonoBehaviour {

  public float targetMaxVolume = 1.0f;

  public abstract float getMultiplier() ;

  public float getVolume() {
    if (this.gameObject.tag == "SoundBGM") {
      targetMaxVolume = VolumeController.GetVolume(VolumeController.VolumeType.BGM);
    }
    else if (this.gameObject.tag == "SoundSFX") {
      targetMaxVolume = VolumeController.GetVolume(VolumeController.VolumeType.SFX);
    }
    else if (this.gameObject.tag == "SoundUI") {
      targetMaxVolume = VolumeController.GetVolume(VolumeController.VolumeType.UI);
    }
    return(targetMaxVolume * getMultiplier()) ;
  }
}
