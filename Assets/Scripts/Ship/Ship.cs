﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
	[Header("Ship Components")]
	public ShipEnergy shipEnergy;
	public ShipHealth shipHealth;
	public ShipMovement shipMovement;
	public ShipShield shipShield;
	public ShipWeapon shipWeapon;
	public GameObject selectRing;
	public ShipFieldOfView shipFieldOfView;
	public ShipStatusHandler shipStatusHandler;
	public Healthbar healthbar;
	public readonly bool isShip = true;

	public bool isSelected {get; private set;}

	[Header("Ship ID")]
	public int shipOwnerId = 1;

	[Header("Name")]
	public string shipName;

	[Header("Out of Bound")]
	public int maxOutOfBoundTurn = 3;
	private int currentOutOfBoundTurn = 0;
	private bool isAlmostOutofBound = false;
	private bool isOutOfBound = false;
	public int outOfBoundTurnRemain {
		get{
			return maxOutOfBoundTurn - currentOutOfBoundTurn;
		}
	}

	//static event
	public delegate void OnFireEvent(Ship ship, Ship targetShip);
	public delegate void OnFireEventWithResult(Ship ship, Ship targetShip, FireResult result);
	public static event OnFireEvent OnWeaponFire;
	public static event OnFireEventWithResult OnWeaponFireWithResult;
	public delegate void OnHitEvent(Ship ship, float value);
	public static event OnHitEvent OnShieldHit;
	public static event OnHitEvent OnHealthHit;
	public delegate void StatusEvent(Ship ship);
	public static event StatusEvent OnStartMoving;
	public static event StatusEvent OnStopMoving;
	public static event StatusEvent OnDead;
	public static event StatusEvent OnShow;
	public static event StatusEvent OnHide;
    public delegate void DestroyEvent(Ship ship);
    public static event DestroyEvent OnShipDestroyed;

	//Ship Activation
	public bool isActivate { get; private set; }

	//Events
	public delegate void ComponentAction();
	public event ComponentAction OnEnergyChange;
	public event ComponentAction OnSelected;
	public event ComponentAction OnDeselected;
	public delegate void OnValueChangeEvent(float value);
	public event OnValueChangeEvent OnShieldChange;
	public event OnValueChangeEvent OnHealthChange;

	void Awake() {
		CheckShipComponent();
		selectRing.SetActive(false);
		isActivate = false;
		isSelected = false;
		GameStateController.OnStateActivate += OnStateActivate;
	}

	void Start() {

	}

	void Update() {

	}

	void OnDestroy() {
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	private void CheckShipComponent() {
		Debug.Assert(shipEnergy, gameObject.name + ": shipEnergy not found!");
		Debug.Assert(shipHealth, gameObject.name + ": shipHealth not found!");
		Debug.Assert(shipMovement, gameObject.name + ": shipMovement not found!");
		Debug.Assert(shipShield, gameObject.name + ": shipShield not found!");
		Debug.Assert(shipWeapon, gameObject.name + ": shipWeapon not found!");
		Debug.Assert(selectRing, gameObject.name + ": selectRing not found!");
		Debug.Assert(shipFieldOfView, gameObject.name + ": shipFieldOfView not found!");
		Debug.Assert(shipStatusHandler, gameObject.name + ": shipStatusHandler not found!");
	}

	public void Select() {
		isSelected = true;
		selectRing.SetActive(true);

		if(OnSelected != null){
			OnSelected();
		}
	}

	public void Deselect() {
		isSelected = false;
		selectRing.SetActive(false);

		if(OnDeselected != null){
			OnDeselected();
		}
	}

	public void SetStartAndSpeedPosition(Vector3 start, Vector3 speed){
        shipMovement.SetStartAndSpeedPosition(start, speed);
    }

	public void SetTargetPosition(Vector3 position) {
		position = new Vector3(position.x, position.y, transform.position.z);
		shipMovement.SetTargetPosition(position);
	}

	public Vector3 GetCurrentSpeed(){
		return shipMovement.GetCurrentSpeed();
	}

	public float GetCurrentSpeedRatio(){
		return shipMovement.GetCurrentSpeedRatio();
	}

	private void OnStateActivate(GameStateController.GameStates state) {
		switch (state) {
			case GameStateController.GameStates.Planning:
				DeactivateShip();
				break;
			case GameStateController.GameStates.Execute:
				ActivateShip();
				break;
			case GameStateController.GameStates.Ending:
				DeactivateShip();
				break;
			default:
				break;
		}
	}

	private void ActivateShip() {
		isActivate = true;
		shipMovement.StartMoving();
	}

	private void DeactivateShip() {
		shipMovement.StopMoving();
		isActivate = false;
	}

	public float GetMaxEnergyPool() {
		return shipEnergy.GetEnergyPool();
	}

	public float GetRemainingEnergy() {
		return shipEnergy.GetRemainingEnergy();
	}

	public float GetCurrentEnergy(ShipEnergy.EnergyType type) {
		return shipEnergy.GetCurrentEnergy(type);
	}

	public float GetRawCurrentEnergy(ShipEnergy.EnergyType type) {
		return shipEnergy.GetRawCurrentEnergy(type);
	}

	public float GetMaxEnergy(ShipEnergy.EnergyType type) {
		return shipEnergy.GetMaxEnergy(type);
	}

	public bool SetEnergy(float[] value) {
		return shipEnergy.SetEnergy(value);
	}

	public void ApplyEnergyMultiplyer(float value){
		shipEnergy.ApplyEnergyMultiplyer(value);
	}

	public void ResetEnergyMultiplyer(){
		shipEnergy.ResetEnergyMultiplyer();
	}

	public void EnergyChange() {
		if (OnEnergyChange != null) {
			OnEnergyChange();
		}
	}

	public float GetCurrentShield() {
		return shipShield.GetCurrentShield();
	}

	public float GetMaxShield() {
		return shipShield.GetMaxShield();
	}

	public float GetShieldRatio(){
		return Mathf.Clamp01(GetCurrentShield() / GetMaxShield());
	}

	public float GetShieldFocusAngle(){
		return shipShield.GetFocusAngle();
	}

	public bool SetShieldFocusAngle(float angle){
		return shipShield.SetFocusAngle(angle);
	}

	public float GetCurrentHealth() {
		return shipHealth.currentHealth;
	}

	public float GetMaxHealth() {
		return shipHealth.maxHealth;
	}

	public float GetHealthRatio(){
		return Mathf.Clamp01((float)GetCurrentHealth() / (float)GetMaxHealth());
	}

	public bool Attack(Ship target, float damage = 0) {
		if (isActivate && GetCurrentEnergy(ShipEnergy.EnergyType.Weapon) > 0 && target &&
			target.GetCurrentHealth() > 0) {
			float angle = Mathf.Atan2(transform.position.y - target.transform.position.y,
				transform.position.x - target.transform.position.x) * Mathf.Rad2Deg;
			WeaponFireEvent(target);
            AudioHandler.CreateSound("SFX_ShipFire", this.gameObject);
			
			FireResult fireResult = new FireResult();
			fireResult.beforeHp = target.GetCurrentHealth();
			
			target.RecieveAttack(this, damage, angle);
			
			fireResult.afterHp = target.GetCurrentHealth();
			fireResult.damage = damage;
			WeaponFireEventWithResult(this, target, fireResult);
			return true;
		}
		else {
			return false;
		}
	}

	public void RecieveAttack(Ship fromShip, float damage, float angle, bool isPenetrate = false) {
		float realDamage = damage;
		if (damage > 0 && !isPenetrate) {
			realDamage = shipShield.ReceiveAttack(damage, angle);
		}
		if (realDamage > 0) {
			shipHealth.RecieveAttack(realDamage);
		}
		
		if (GetCurrentHealth() <= 0 && fromShip == null)
			Dead();
	}

	public void SetAttackTarget(Ship ship){
		shipWeapon.SetShipTarget(ship);
	}

	public void Dead() {
		DeadEvent();
        if (OnShipDestroyed != null){
            OnShipDestroyed(this);
        }
		gameObject.SetActive(false);
	}

	public void WeaponFireEvent(Ship targetShip) {
		if (OnWeaponFire != null) {
			OnWeaponFire(this, targetShip);
		}
	}
	
	public static void WeaponFireEventWithResult(Ship ship,Ship targetShip, FireResult result) {
		if (OnWeaponFireWithResult != null) {
			OnWeaponFireWithResult(ship, targetShip, result);
		}
	}

	public void ShieldHitEvent(float value) {
		if (OnShieldHit != null) {
			OnShieldHit(this, value);
		}
	}

	public void HpHitEvent(float value) {
		if (OnHealthHit != null) {
			OnHealthHit(this, value);
		}
	}

	public void ShieldChangeEvent() {
		if (OnShieldChange != null) {
			OnShieldChange(GetCurrentShield());
		}
	}

	public void HpChangeEvent() {
		if (OnHealthChange != null) {
			OnHealthChange(GetCurrentHealth());
		}
	}
    
    public static void ShipDestroyed(Ship ship) {		
	    OnShipDestroyed(ship);		
	}

	public void StartMovingEvent() {
		if (OnStartMoving != null) {
			OnStartMoving(this);
		}
	}

	public void StopMovingEvent() {
		if (OnStopMoving != null) {
			OnStopMoving(this);
		}
	}

	public void DeadEvent(){
		if(OnDead != null){
			OnDead(this);
		}
	}
	
	public static void DeadEvent(Ship ship){
		if(OnDead != null){
			OnDead(ship);
		}
	}

	public void ShowEvent(Ship ship){
		if(OnShow != null){
			OnShow(ship);
		}
	}

	public void HideEvent(Ship ship){
		if(OnHide != null){
			OnHide(ship);
		}
	}

	public void AllowToMoveByCalculation(bool value) {
		shipMovement.AllowToMoveByCalculation(value);
	}

	public void AllowTAttackByCalculation(bool value) {
		shipWeapon.AllowToAttackByCalculation(value);
	}

	public void SeenBy(Ship ship){
		shipFieldOfView.SeenBy(ship);
	}

	public void UnseenBy(Ship ship){
		shipFieldOfView.UnseenBy(ship);
	}

	public void ForceHide(){
		shipFieldOfView.ForceHide();
	}

	public void StopForceHide(){
		shipFieldOfView.StopForceHide();
	}

	public void ResetOutOfBoundCount(){
		currentOutOfBoundTurn = 0;
		isOutOfBound = false;
	}

	public void OutOfBound(){
		isOutOfBound = true;
	}

	public void OutOfBoundCheck(){
		if(isOutOfBound){
			currentOutOfBoundTurn += 1;
			if(currentOutOfBoundTurn >= maxOutOfBoundTurn){
				Dead();
			}
			else{
				WarningText.AddOuterOutOfBoundMessage(this, outOfBoundTurnRemain);
			}
		}
		else if(isAlmostOutofBound){
			WarningText.AddInnerOutOfBoundMessage(this);
		}
	}

	public void AlmostOutOfBound(){
		isAlmostOutofBound = true;
	}

	public void ResetAlmostOutOfBound(){
		isAlmostOutofBound = false;
	}

	public bool IsSeen(){
		return shipFieldOfView.isSeen;
	}

	public void EnterIonField(IonField field){
		shipStatusHandler.EnterIonField(field);
	}

	public void ExitIonField(IonField field){
		shipStatusHandler.ExitIonField(field);
	}

	public void SetHealthBarToIonFieldColor(){
        healthbar.SetIonFieldColor();
    }

    public void SetHealthBarToNormalColor(){
        healthbar.SetNormalColor();
    }
}
