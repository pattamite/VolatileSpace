﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipModel : MonoBehaviour {

	public bool disableTrail = false;

	// Use this for initialization
	void Start () {
		if(disableTrail) DisableAllTrail();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DisableAllTrail(){
		foreach(Transform child in transform){
			TrailRenderer trail = child.gameObject.GetComponent<TrailRenderer>();
			if(trail) trail.enabled = false;
		}
	}
}
