﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHealth : ShipComponent {
	[Header("Health Info")]
	[Range(1f, 300f)] public float maxHealth = 1;
	public float currentHealth;

	public float CurrentHealth
	{
		get { return currentHealth; }
		set
		{
			currentHealth = value; 
			ship.HpChangeEvent();
		}
	}

	protected override void Awake() {
		base.Awake();

		currentHealth = maxHealth;
	}

	void Start() {

	}

	void Update() {

	}

	public void RecieveAttack(float damage) {
		currentHealth -= damage;
		ship.HpHitEvent(damage);
		ship.HpChangeEvent();

		if (currentHealth <= 0) {
			currentHealth = 0;
			AudioHandler.CreateSound("SFX_ShipDestroyed", ship.gameObject);
//			ship.Dead();
		}
		else {
			AudioHandler.CreateSound("SFX_ShipHit", ship.gameObject);
		}
	}
}
