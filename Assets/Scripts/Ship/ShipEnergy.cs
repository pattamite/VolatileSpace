﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEnergy : ShipComponent {

	[Header("Energy Pool")]
	[Range(0f, 200f)] public float maxEnergy = 100;

	[Header("Energy Limit")]
	[Range(0f, 200f)] public float maxShield = 100;
	[Range(0f, 200f)] public float maxWeapon = 100;
	[Range(0f, 200f)] public float maxManeuver = 100;

	public enum EnergyType { Shield, Weapon, Maneuver }

	private float currentShield;
	private float currentWeapon;
	private float currentManeuver;
	private float energyMultiplyer = 1f;

	protected override void Awake() {
		base.Awake();
		SetInitialValue();
	}

	void Start() {

	}

	void Update() {

	}

	private void SetInitialValue() {
		currentShield = maxShield / 3;
		currentWeapon = maxWeapon / 3;
		currentManeuver = maxManeuver / 3;
	}

	public float GetEnergyPool() {
		return maxEnergy;
	}

	public float GetRemainingEnergy() {
		return maxEnergy - (currentShield + currentWeapon + currentManeuver);
	}

	public float GetCurrentEnergy(EnergyType type) {
		switch (type) {
			case EnergyType.Shield:
				return currentShield * energyMultiplyer;
			case EnergyType.Weapon:
				return currentWeapon * energyMultiplyer;
			case EnergyType.Maneuver:
				return currentManeuver * energyMultiplyer;
			default:
				return 0;
		}
	}

	public float GetRawCurrentEnergy(EnergyType type) {
		switch (type) {
			case EnergyType.Shield:
				return currentShield;
			case EnergyType.Weapon:
				return currentWeapon;
			case EnergyType.Maneuver:
				return currentManeuver;
			default:
				return 0;
		}
	}

	public float GetMaxEnergy(EnergyType type) {
		switch (type) {
			case EnergyType.Shield:
				return maxShield;
			case EnergyType.Weapon:
				return maxWeapon;
			case EnergyType.Maneuver:
				return maxManeuver;
			default:
				return 0;
		}
	}

	public bool SetEnergy(float[] value) {
		if (value.Length < System.Enum.GetValues(typeof(EnergyType)).Length) {
			Debug.LogError(gameObject.name + ": not enough value to set energy.");
			return false;
		}

		float shield = value[(int) EnergyType.Shield];
		float weapon = value[(int) EnergyType.Weapon];
		float manuever = value[(int) EnergyType.Maneuver];
		float total = shield + weapon + manuever;

		if (total > maxEnergy) {
			shield = (shield / total) * maxEnergy;
			weapon = (weapon / total) * maxEnergy;
			manuever = maxEnergy - (shield + weapon);
		}

		bool isEnergyChange = false;
		if (shield != currentShield || weapon != currentWeapon || manuever != currentManeuver) {
			isEnergyChange = true;
		}

		currentShield = Mathf.Clamp(shield, 0, maxShield);
		currentWeapon = Mathf.Clamp(weapon, 0, maxWeapon);
		currentManeuver = Mathf.Clamp(manuever, 0, maxManeuver);

		if (isEnergyChange) {
			ship.EnergyChange();
		}

		return true;
	}

	public void ApplyEnergyMultiplyer(float value) {
		energyMultiplyer = value;
	}

	public void ResetEnergyMultiplyer() {
		energyMultiplyer = 1;
	}
}