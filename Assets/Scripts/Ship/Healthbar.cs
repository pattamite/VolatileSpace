﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : ShipComponent {
    public Image shieldBarFill;
    public Image healthBarFill;

    private Color normalColor;
    public Color ionFieldColor;
	// Use this for initialization
	void Start () {
        CheckShipComponent();

        healthBarFill.fillAmount = ship.GetHealthRatio();
        shieldBarFill.fillAmount = ship.GetShieldRatio();

        normalColor = healthBarFill.color;

        ship.OnHealthChange += HealthChange;
        ship.OnShieldChange += ShieldChange;
    }
	
	void Update () {
		
	}

    void OnDestroy(){
        ship.OnHealthChange -= HealthChange;
        ship.OnShieldChange -= ShieldChange;
    }

    private void CheckShipComponent() {
		Debug.Assert(shieldBarFill, gameObject.name + ": shieldBarFill not found!");
		Debug.Assert(healthBarFill, gameObject.name + ": healthBarFill not found!");
	}

    void HealthChange(float value){
        healthBarFill.fillAmount = ship.GetHealthRatio();
    }

    void ShieldChange(float value){
        shieldBarFill.fillAmount = ship.GetShieldRatio();
    }

    public void SetIonFieldColor(){
        healthBarFill.color = ionFieldColor;
    }

    public void SetNormalColor(){
        healthBarFill.color = normalColor;
    }
}
