﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipComponent : MonoBehaviour {
	public Ship ship {get; private set;}

	protected virtual void Awake() {
		GetShip();
		Debug.Assert(ship, gameObject.name + ": parent ship not found.");
	}

	protected void GetShip(){
		Transform current = transform;
		while(current.parent){
			ship = current.parent.GetComponent<Ship>();
			if(ship) break;
			else current = current.parent;
		}
	}
}