﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipWeapon : ShipComponent {
    [Header("Attack Info")]
    [Curve(0, 0, 100, 100
    )] public AnimationCurve attackDamagePerSecond;
    public float intervalTime = 0.3f;

    [Header("Attack Detection Info")]
    [Range(0f, 100f)] public float viewRadius = 5f;
    [Range(0f, 360f)] public float viewAngle = 90f;
    [Range(0.01f, 1f)] public float refreshTime = 0.1f;
    [Range(0, 5)] public int edgeResolveIterations = 4;
    [Range(0f, 1f)] public float edgeDistanceThreshold = 0.5f;
    [Range(0f, 5f)] public float meshResolution = 2.5f;
    public LayerMask targetMask;
    private float outOfDelayTime;

    [Header("Component")]
    public MeshFilter viewMeshFilter;
    private Mesh viewMesh;
    private LineRenderer lineRenderer;

    //TargetList
    public List<Transform> visibleTargets = new List<Transform>();
    public List<Ship> shipTargetList;
    public Ship shipTarget { get; private set; }

    //Flags
    public bool isAllowToAttackByCalcucation { get; private set; }

    protected override void Awake() {
        base.Awake();
        lineRenderer = GetComponent<LineRenderer>();
        isAllowToAttackByCalcucation = true;
        shipTargetList = new List<Ship>();
    }

    void Start() {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        StartCoroutine("FindTargetsWithDelay");
    }

    void Update() {
        CheckAttack();
        DrawAttackTargetLine();
    }

    void LateUpdate() {
        DrawFieldOfView();
    }

    /*private void OnTriggerEnter(Collider collision) {
        ShipHitbox hitbox = collision.GetComponent<ShipHitbox>();
        Ship otherShip = hitbox ? hitbox.ship : null;
        if (GameController.IsShipAlive(otherShip) && otherShip.shipOwnerId != ship.shipOwnerId) {
            shipTargetList.Add(otherShip);
        }
    }*/

    /*private void OnTriggerExit(Collider collision) {
        ShipHitbox hitbox = collision.GetComponent<ShipHitbox>();
        Ship otherShip = hitbox ? hitbox.ship : null;
        if (GameController.IsShipAlive(otherShip) && shipTargetList.Contains(otherShip)) {
            shipTargetList.Remove(otherShip);
        }
    }*/

    public void SetShipTarget(Ship otherShip) {
        if (GameController.IsShipAlive(otherShip) && otherShip.shipOwnerId != ship.shipOwnerId) {
            shipTarget = otherShip;
        }
    }

    /*private void OnTriggerStay(Collider collision) {
        if (ship.isActivate) {
            ShipHitbox hitbox = collision.GetComponent<ShipHitbox>();
            Ship otherShip = hitbox ? hitbox.ship : null;
            if (isAllowToAttackByCalcucation && otherShip && otherShip.shipOwnerId != ship.shipOwnerId && outOfDelayTime <= Time.time &&
                ship.Attack(otherShip, damagePerShotPerEnergyUnit * ship.GetCurrentEnergy(ShipEnergy.EnergyType.Weapon))) {
                outOfDelayTime = Time.time + intervalTime;

                //				StopCoroutine(RemoveDraw());
                //				DrawFire(otherShip);
                //				StartCoroutine(RemoveDraw());
            }
        }
    }*/

    private void CheckAttack() {
        if (ship.isActivate) {
            Ship target = GetTargetShip();
            //print(isAllowToAttackByCalcucation + " / " + (target != false) + " / " + (outOfDelayTime <= Time.time));
            if (isAllowToAttackByCalcucation && target && outOfDelayTime <= Time.time &&
                ship.Attack(target, attackDamagePerSecond.Evaluate(ship.GetCurrentEnergy(ShipEnergy.EnergyType.Weapon) * intervalTime))) {
                outOfDelayTime = Time.time + intervalTime;
            }
                
        }
    }

    private void DrawAttackTargetLine() {
        if (shipTarget && GameController.IsShipAlive(shipTarget) && shipTarget.IsSeen()) {
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, ship.transform.position);
            lineRenderer.SetPosition(1, shipTarget.transform.position);
        }
        else {
            lineRenderer.positionCount = 0;
        }
    }

    public IEnumerator RemoveDraw() {
        yield return new WaitForSeconds(0.1f);
        lineRenderer.positionCount = 0;
        yield return null;
    }

    public void AllowToAttackByCalculation(bool value) {
        isAllowToAttackByCalcucation = value;
    }

    private Ship GetTargetShip() {
        //check target selected
        if (GameController.IsShipAlive(shipTarget) && shipTargetList.Contains(shipTarget) && shipTarget.IsSeen()) {
            return shipTarget;
        }

        //clear dead ship
        for (int i = shipTargetList.Count - 1; i >= 0; i--) {
            if (!GameController.IsShipAlive(shipTargetList[i])) {
                shipTargetList.RemoveAt(i);
            }
        }

        //select first seen target
        for (int i = 0; i < shipTargetList.Count; i++) {
            if (shipTargetList[i].IsSeen()) {
                return shipTargetList[i];
            }
        }

        return null;
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
        if (!angleIsGlobal) {
            angleInDegrees += transform.eulerAngles.z;
        }
        return new Vector3(Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0);
    }

    IEnumerator FindTargetsWithDelay() {
        while (true) {
            yield return new WaitForSeconds(refreshTime);
            FindVisibleTargets();
        }
    }

    private void FindVisibleTargets() {
        visibleTargets.Clear();
        shipTargetList.Clear();
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++) {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.right, dirToTarget) < viewAngle / 2) {
                float distance = Vector3.Distance(transform.position, target.position);

                RaycastHit hit;
                if (Physics.Raycast(transform.position, dirToTarget, out hit, distance, targetMask) &&
                    hit.collider.transform == target) {
                    ShipHitbox hitbox = target.GetComponent<ShipHitbox>();
                    Ship otherShip = hitbox ? hitbox.ship : null;
                    if (GameController.IsShipAlive(otherShip) && otherShip.shipOwnerId != ship.shipOwnerId) {
                        shipTargetList.Add(otherShip);
                    }
                    visibleTargets.Add(target);
                }
            }
        }
    }

    void DrawFieldOfView() {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        ViewCastInfo oldViewCast = new ViewCastInfo();

        for (int i = 0; i < stepCount; i++) {
            float angle = transform.eulerAngles.z - (viewAngle / 2) + (stepAngleSize * i);
            ViewCastInfo newViewCast = ViewCast(angle);

            if (i > 0) {
                bool isEdgeDistanceExeeded = Mathf.Abs(oldViewCast.distance - newViewCast.distance) > edgeDistanceThreshold;
                if (oldViewCast.hit != newViewCast.hit ||
                    (oldViewCast.hit && newViewCast.hit && isEdgeDistanceExeeded)) {
                    EdgeInfo edge = FindEdge(oldViewCast, newViewCast);
                    if (edge.pointA != Vector3.zero) {
                        viewPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero) {
                        viewPoints.Add(edge.pointB);
                    }
                }
            }

            viewPoints.Add(newViewCast.point);
            oldViewCast = newViewCast;
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++) {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2) {
                triangles[i * 3] = 0;
                triangles[(i * 3) + 1] = i + 2;
                triangles[(i * 3) + 2] = i + 1;
            }
        }

        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast) {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        for (int i = 0; i < edgeResolveIterations; i++) {
            float angle = (minAngle + maxAngle) / 2;
            ViewCastInfo newViewCast = ViewCast(angle);
            bool isEdgeDistanceExeeded = Mathf.Abs(minViewCast.distance - maxViewCast.distance) > edgeDistanceThreshold;

            if (newViewCast.hit == minViewCast.hit && !isEdgeDistanceExeeded) {
                minAngle = angle;
                minPoint = newViewCast.point;
            }
            else {
                maxAngle = angle;
                maxPoint = newViewCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    ViewCastInfo ViewCast(float globalAngle) {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, dir, out hit, viewRadius, targetMask)) {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else {
            return new ViewCastInfo(false, transform.position + (dir * viewRadius),
                viewRadius, globalAngle);
        }
    }

    public struct ViewCastInfo {
        public bool hit;
        public Vector3 point;
        public float distance;
        public float angle;

        public ViewCastInfo(bool hit, Vector3 point, float distance, float angle) {
            this.hit = hit;
            this.point = point;
            this.distance = distance;
            this.angle = angle;
        }
    }

    public struct EdgeInfo {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 pointA, Vector3 pointB) {
            this.pointA = pointA;
            this.pointB = pointB;
        }
    }
}

public struct FireResult {
    public float beforeHp;
    public float afterHp;
    public float damage;
}
