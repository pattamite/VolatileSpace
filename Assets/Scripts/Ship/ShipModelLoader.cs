﻿ using System;
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipModelLoader : MonoBehaviour
{

	public static ShipModelLoader instance { get; private set; }

	public GameObject[] modelPrfabs;
	public ShieldPanel shieldPanel;
	
	void Awake()
	{
		instance = this;
	}

	void begin(){
		Debug.Assert(shieldPanel, gameObject.name + ": shieldPanel not found!");
	}


	public void ReplaceShipModel(Ship[] ships, int index)
	{
		foreach (var ship in ships)
		{
			ReplaceShipModel(ship, index);
		}

		SettingShieldPanelModel(ships, index);
	}

	private void SettingShieldPanelModel(Ship[] ships, int index)
	{
		GameObject model = Instantiate(modelPrfabs[index]) as GameObject;
		foreach (var trailRenderer in model.GetComponentsInChildren<TrailRenderer>())
		{
			trailRenderer.gameObject.SetActive(false);
		}
		shieldPanel.SetPlayerModel(model, ships[0].shipOwnerId);
	}
	
	public void ReplaceShipModel(Ship ship, int index)
	{
		Transform model = ship.transform.Find("Model").Find("MeshModel");
		ReplaceModel(model, index);
	}

	public void ReplaceShipModelAndRemoveTrail(Ship ship, int index)
	{
		ReplaceShipModel(ship, index);
		foreach (var trailRenderer in ship.GetComponentsInChildren<TrailRenderer>())
		{
			trailRenderer.gameObject.SetActive(false);
		}
	}
	
	public void ReplaceModelAndRemoveTrail(Transform parent, int index)
	{
		ReplaceModel(parent, index);
		foreach (var trailRenderer in parent.GetComponentsInChildren<TrailRenderer>())
		{
			trailRenderer.gameObject.SetActive(false);
		}
	}

	public void ReplaceModel(Transform parent, int index) 
	{
		Debug.Log("ReplaceModel transform:" + parent.gameObject.name + " , index:" + index);
		if (index >= modelPrfabs.Length || index < 0)
		{
			Debug.Log("index must be more than -1 and less than " + modelPrfabs.Length);
			return;
		}

		foreach (Transform child in parent)
		{
			Destroy(child.gameObject);
		}

		GameObject modelObj =  Instantiate(modelPrfabs[index], parent);
		modelObj.transform.localPosition = Vector3.zero;
		modelObj.transform.localRotation = Quaternion.identity;
	}
	
	
}
