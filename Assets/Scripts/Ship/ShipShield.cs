﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ShipShield : ShipComponent {
	[Header("Shield Values")]
	public float maxShield;
	public float startShield;
	public float currentShield { get; private set; }

	[Header("Shield Regeneration by Energy")]
	[Curve(0, 0, 100, 100)] public AnimationCurve shieldRegen;

	[Header("Shield multiplyer by angle")]
	[Curve(0, 0, 180, 1)] public AnimationCurve shieldMultiplyerByAngle;

	[Header("Particles")]
	public GameObject shieldVisual;
	private List<ParticleSystem> particles = new List<ParticleSystem>();
	[Range(0.1f, 0.5f)] public float maxVisualSize = 0.3f;

	//angle
	public float focusAngle { get; private set; }

	public float CurrentShield
	{
		get { return currentShield; }
		set
		{
			currentShield = value; 
			ship.ShieldChangeEvent();
		}
	}

	private void OnValidate() {
		maxShield = Mathf.Clamp(maxShield, 0, int.MaxValue);
		startShield = Mathf.Clamp(startShield, 0, maxShield);
	}

	protected override void Awake() {
		base.Awake();
		currentShield = startShield;
		focusAngle = 0;
	}

	void Start() {
		GameStateController.OnStateActivate += OnStateActivate;

		foreach(Transform child in shieldVisual.transform){
			ParticleSystem particle = child.GetComponent<ParticleSystem>();
			if(particle){
				particles.Add(particle);
			}
		}

		UpdateVisual();
	}

	void Update() {

	}

	void OnDestroy() {
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	public void OnStateActivate(GameStateController.GameStates state) {
		switch (state) {
			case GameStateController.GameStates.Execute:
				RegenShield();
				break;
			default:
				break;
		}
	}

	public float GetCurrentShield() {
		return currentShield;
	}

	public float GetMaxShield(){
		return maxShield;
	}

	public float GetCurrentShieldByAngle(float angle) {
		return currentShield * shieldMultiplyerByAngle.Evaluate(angle);
	}

	public float GetFocusAngle(){
		
		return focusAngle;
	}

	public bool SetFocusAngle(float value) {
		focusAngle = value;
		shieldVisual.transform.localEulerAngles = new Vector3(0, 0, value);
		return true;
	}

	public float ReceiveAttack(float damage, float attackAngle) {
		float shieldBeforeHit = currentShield;

		//calculate attack angle to ship angle to delta from focus angle
		float realAngle = Mathf.Abs(Mathf.DeltaAngle(attackAngle - ship.transform.eulerAngles.z, focusAngle));
		float shield = GetCurrentShieldByAngle(realAngle);
		float realDamage = Mathf.Clamp(damage - shield, 0, float.MaxValue);
		float shieldLoss = Mathf.Clamp(damage - realDamage, 0, currentShield);

		/*Debug.Log(string.Format("{0} Shield : Got hit {1} damage at {2} degree. Real Damage = {3} - {4} = {5}. Shield Left = {6} - {7} = {8}.",
			ship.gameObject, damage, realAngle, damage, shield, realDamage,
			currentShield, shieldLoss, Mathf.Clamp(currentShield - shieldLoss, 0, float.MaxValue)));*/

		currentShield = Mathf.Clamp(currentShield - shieldLoss, 0, float.MaxValue);
		if(currentShield > 0) {
			AudioHandler.CreateSound("SFX_ShieldHit", transform.position);
		}
		else if(shieldBeforeHit > 0) {
			AudioHandler.CreateSound("SFX_ShieldDown", transform.position);
		}
		ship.ShieldHitEvent(shieldLoss);
		ship.ShieldChangeEvent();
		UpdateVisual();

		return realDamage;
	}

	private void RegenShield() {
		currentShield = Mathf.Clamp(currentShield + shieldRegen.Evaluate(ship.GetCurrentEnergy(ShipEnergy.EnergyType.Shield)), 0, maxShield);
		ship.ShieldChangeEvent();
		UpdateVisual();
	}

	private void UpdateVisual(){
		foreach(ParticleSystem particle in particles){
			particle.Clear();

			var main = particle.main;
			main.startSize = maxVisualSize * ship.GetShieldRatio();

			particle.Simulate(5f);
		}
	}
}
