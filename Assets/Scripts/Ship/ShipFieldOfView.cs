﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipFieldOfView : ShipComponent {

	[Header("Objects To Hide")]
	public GameObject[] models;
	public LineRenderer flightPath;
	public LineRenderer directionPath;
	public LineRenderer rangePath;
	public Canvas healthbarCanvas;
	public GameObject weaponVisualizer;
	public GameObject shieldVisualizer;

	[Header("Parameters")]
	[Range(0f, 3f)] public float timeUntilHide = 0.5f;

	public bool isSeen { get; private set; }
	private float timeLeft;
	private bool isShowingShipAllTime;
	private List<Ship> seenByShips = new List<Ship>();
	private bool forceHide = false;

	void Start() {
		GameStateController.OnStateActivate += OnStateActivate;
		Ship.OnDead += OnShipDead;
	}

	void Update() {
		
	}
	void OnDestroy() {
		GameStateController.OnStateActivate -= OnStateActivate;
		Ship.OnDead -= OnShipDead;
		StopCoroutine("DelayHideShip");
	}

	public void SeenBy(Ship ship) {
		if (!seenByShips.Contains(ship)) {
			seenByShips.Add(ship);
		}

		if (seenByShips.Count > 0 && !forceHide) {
			StopCoroutine("DelayHideShip");
			ShowShip();
		}
	}

	public void UnseenBy(Ship ship) {
		if (seenByShips.Contains(ship)) {
			seenByShips.Remove(ship);
		}
		//todo
		if (seenByShips.Count <= 0 && this.isActiveAndEnabled) {
			StartCoroutine("DelayHideShip");
		}
	}

	private IEnumerator DelayHideShip() {
		yield return new WaitForSeconds(timeUntilHide);
		HideShip();
	}

	private void HideShip() {
		isSeen = false;
		ship.HideEvent(ship);
		SetShowShipVisual(false);
	}

	private void ShowShip() {
		isSeen = true;
		ship.ShowEvent(ship);
		SetShowShipVisual(true);
	}

	private void SetShowShipVisual(bool value) {
		//if(!GameController.IsPlayerId(ship.shipOwnerId)){
		if (!GameController.isOfflineModeActivate && !GameController.IsPlayerId(ship.shipOwnerId)) {
			foreach (GameObject mesh in models) {
				if (mesh) {
					mesh.SetActive(value);
				}
			}

			flightPath.enabled = value;
			directionPath.enabled = value;
			rangePath.enabled = value;
			healthbarCanvas.enabled = value;
			weaponVisualizer.SetActive(value);
			shieldVisualizer.SetActive(value);
		}
	}

	private void OnTriggerEnter(Collider collision) {
		ShipHitbox hitbox = collision.GetComponent<ShipHitbox>();
		Ship otherShip = hitbox ? hitbox.ship : null;
		if (otherShip && otherShip.shipOwnerId != ship.shipOwnerId) {
			otherShip.SeenBy(ship);
		}
	}

	private void OnTriggerExit(Collider collision) {
		ShipHitbox hitbox = collision.GetComponent<ShipHitbox>();
		Ship otherShip = hitbox ? hitbox.ship : null;
		if (otherShip && otherShip.shipOwnerId != ship.shipOwnerId) {
			otherShip.UnseenBy(ship);
		}
	}

	private void OnStateActivate(GameStateController.GameStates state) {
		switch (state) {
			case GameStateController.GameStates.NetworkInit:
				HideShip();
				break;
			default:
				break;
		}
	}

	private void OnShipDead(Ship ship) {
		UnseenBy(ship);
	}

	public void ForceHide() {
		forceHide = true;
		HideShip();
	}

	public void StopForceHide() {
		forceHide = false;
		if (seenByShips.Count > 0) ShowShip();
	}
}