﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipStatusHandler : ShipComponent {

	private List<IonField> ionFieldList = new List<IonField>();
	private float currentIonFieldMultiplyer = 1f;

	// Use this for initialization
	void Start () {
		GameStateController.OnStateActivate += OnStateActivate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDestroy(){
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	public void EnterIonField(IonField field){
		if(field && !ionFieldList.Contains(field) && GameController.IsShipAlive(ship)){
			ionFieldList.Add(field);
			IonField.CheckIonField(ship, ionFieldList);
		}
	}

	public void ExitIonField(IonField field){
		if(field && ionFieldList.Contains(field) && GameController.IsShipAlive(ship)){
			ionFieldList.Remove(field);
			IonField.CheckIonField(ship, ionFieldList);
		}
	}

	private void EndTurnChecking(){
		if(GameController.IsShipAlive(ship)){
			IonField.CheckEndTurnIonField(ship, ionFieldList);
		}
	}

	private void OnStateActivate(GameStateController.GameStates state){
		switch(state){
			case GameStateController.GameStates.Ending:
				EndTurnChecking();
				break;
			default:
				break;
		}
	}

	
}
