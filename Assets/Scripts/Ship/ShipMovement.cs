﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : ShipComponent {

    [Header("Range Limit")]
    [Range(0.001f, 1f)] public float minSpeed;
    [Curve(0, 0, 100, 20)] public AnimationCurve maxSpeedByEnergy;
    [Curve(0, 0, 100, 180)] public AnimationCurve maxAngleByEnergy;
    [Curve(0, 0, 100, 1)] public AnimationCurve maxAngleMultiplyerByStartSpeed;
    [Curve(0, 0, 100, 1)] public AnimationCurve maxSpeedMultiplyerByAngle;

    [Header("Preview Line")]
    [Range(2, 200)] public int previewSampleRate;
    [Range(0f, 10f)] public float directionLineRange;
    [Range(1, 10)] public int directionLineDotFreqeuncy;
    [Range(0.5f, 20f)] public float rangeSampleDegree;

    //position & speed
    public Vector3 startPosition { get; private set; }
    public Vector3 speedPosition { get; private set; }
    public Vector3 targetPosition { get; private set; }
    private float lastTurnSpeed;

    //flag
    public bool isMoving { get; private set; }
    public bool isAllowToMoveByCalcucation { get; private set; }

    //Component
    public LineRenderer movementLine;
    public LineRenderer directionLine;
    public LineRenderer rangeLine;

    void OnValidate() {

    }

    protected override void Awake() {
        base.Awake();

        isMoving = false;
        isAllowToMoveByCalcucation = true;
        lastTurnSpeed = minSpeed;
        ResetPoints();
    }

    void Start() {
        ship.OnEnergyChange += OnEnergyChange;
        ship.OnSelected += OnSelected;
        ship.OnDeselected += OnDeselected;
        GameStateController.OnStateActivate += OnStateActivate;

        Debug.Assert(movementLine, gameObject.name + ": movementLine not found!");
        Debug.Assert(directionLine, gameObject.name + ": directionLine not found!");
        Debug.Assert(rangeLine, gameObject.name + ": rangeLine not found!");
    }

    void Update() {
        if (isMoving) {
            DrawMovementLine(GetCurrentTimeRatio());
        }
    }

    void FixedUpdate() {
        Movement(GetCurrentTimeRatio());
    }

    void OnDestroy() {
        ship.OnEnergyChange -= OnEnergyChange;
        ship.OnSelected -= OnSelected;
        ship.OnDeselected -= OnDeselected;
        GameStateController.OnStateActivate -= OnStateActivate;
    }

    private void OnEnergyChange() {
        SetTargetPosition(targetPosition);
    }

    private void OnStateActivate(GameStateController.GameStates state) {
        switch (state) {
            case GameStateController.GameStates.Planning:
                ResetPoints();
                break;
            default:
                break;
        }
    }

    public void SetStartAndSpeedPosition(Vector3 start, Vector3 speed) {
        startPosition = start;
        speedPosition = speed;
    }

    public void SetTargetPosition(Vector3 position) {
        position = new Vector3(position.x, position.y, ship.transform.position.z);
        Vector3 referencePoint = speedPosition;
        float speed = 2 * Vector3.Distance(referencePoint, position);
        float targetAngle = Mathf.Atan2(position.y - referencePoint.y, position.x - referencePoint.x) *
            Mathf.Rad2Deg;
        float deltaAngle = Mathf.DeltaAngle(ship.transform.eulerAngles.z, targetAngle);
        float absDeltaAngle = Mathf.Abs(deltaAngle);
        float currentMaxAngle = GetMaxAngleByStartSpeed(SpeedCalculation(0).magnitude);
        float currentMaxSpeed = GetMaxSpeedByAngle(absDeltaAngle, currentMaxAngle);

        if (absDeltaAngle <= currentMaxAngle && speed <= currentMaxSpeed && speed >= minSpeed) {
            targetPosition = position;
        }
        else {
            absDeltaAngle = Mathf.Clamp(absDeltaAngle, 0, currentMaxAngle);
            deltaAngle = (deltaAngle > 0 ? 1 : -1) * absDeltaAngle;
            speed = Mathf.Clamp(speed, minSpeed, GetMaxSpeedByAngle(absDeltaAngle, currentMaxAngle));

            float x = referencePoint.x +
                ((speed / 2) * Mathf.Cos((ship.transform.eulerAngles.z + deltaAngle) * Mathf.Deg2Rad));
            float y = referencePoint.y +
                ((speed / 2) * Mathf.Sin((ship.transform.eulerAngles.z + deltaAngle) * Mathf.Deg2Rad));

            targetPosition = new Vector3(x, y, position.z);
        }

        lastTurnSpeed = SpeedCalculation(1).magnitude;
        DrawMovementLine(0);
    }

    private float GetMaxAngleByStartSpeed(float speed) {
        float speedRatio = Mathf.Clamp((GetCurrentSpeed().magnitude - minSpeed) /
            (maxSpeedByEnergy.Evaluate(ship.GetCurrentEnergy(ShipEnergy.EnergyType.Maneuver)) - minSpeed) * 100, 0, 100);
        return maxAngleByEnergy.Evaluate(ship.GetCurrentEnergy(ShipEnergy.EnergyType.Maneuver)) *
            maxAngleMultiplyerByStartSpeed.Evaluate(speedRatio);
    }

    private float GetMaxSpeedByAngle(float angle, float maxAngle) {
        return maxSpeedByEnergy.Evaluate(ship.GetCurrentEnergy(ShipEnergy.EnergyType.Maneuver));
    }

    public void StartMoving() {
        isMoving = true;
        ship.StartMovingEvent();
    }

    public void StopMoving() {
        isMoving = false;
        ship.StopMovingEvent();
        ResetMovementLine();
    }

    public void Movement(float deltaTime) {
        if (ship.isActivate && isMoving && isAllowToMoveByCalcucation) {
            ship.transform.position = PositionCalculation(GetCurrentTimeRatio());
            ship.transform.eulerAngles = RotationCalculation(GetCurrentTimeRatio());
        }
    }

    private Vector3 PositionCalculation(float timeRatio) {
        timeRatio = Mathf.Clamp(timeRatio, 0, 1);
        float reverseTimeRatio = 1 - timeRatio;

        return (reverseTimeRatio * reverseTimeRatio * startPosition) +
            (2 * timeRatio * reverseTimeRatio * speedPosition) +
            (timeRatio * timeRatio * targetPosition);
    }

    private Vector3 SpeedCalculation(float timeRatio) {
        timeRatio = Mathf.Clamp(timeRatio, 0, 1);
        float reverseTimeRatio = 1 - timeRatio;

        return (2 * reverseTimeRatio * (speedPosition - startPosition)) +
            (2 * timeRatio * (targetPosition - speedPosition));
    }

    private Vector3 RotationCalculation(float timeRatio) {
        timeRatio = Mathf.Clamp(timeRatio, 0, 1);

        Vector3 speed = SpeedCalculation(timeRatio);
        float z = Mathf.Atan2(speed.y, speed.x) * Mathf.Rad2Deg;

        return new Vector3(0, 0, z);
    }

    private void DrawMovementLine(float currentTimeRatio) {
        currentTimeRatio = Mathf.Clamp(currentTimeRatio, 0, 1);
        ResetMovementLine();
        float sampleTime = 1 / (float) previewSampleRate;
        int startPoint = Mathf.FloorToInt(currentTimeRatio / sampleTime);

        Vector3[] points = new Vector3[previewSampleRate - startPoint + 1];
        points[0] = PositionCalculation(currentTimeRatio);
        for (int i = 1; i <= (previewSampleRate - startPoint); i++) {
            points[i] = PositionCalculation(sampleTime * (i + startPoint));
        }

        Vector3 finalMovementPosition = PositionCalculation(1);
        Vector3 directionPosition = finalMovementPosition +
            (SpeedCalculation(1).normalized * directionLineRange);

        directionLine.positionCount = 2;
        directionLine.SetPosition(0, finalMovementPosition);
        directionLine.SetPosition(1, directionPosition);
        directionLine.materials[0].mainTextureScale =
            new Vector2((int) directionLineRange * directionLineDotFreqeuncy, 1);

        movementLine.positionCount = previewSampleRate - startPoint + 1;
        movementLine.SetPositions(points);

        CheckDrawRangeLine();
    }

    private void CheckDrawRangeLine(){
        if (GameStateController.currentGameState == GameStateController.GameStates.Planning &&
            ship.isSelected) {
            DrawRangeLine();
        }
        else {
            RemoveRangeLine();
        }
    }

    private void DrawRangeLine() {
        float currentMaxAngle = GetMaxAngleByStartSpeed(SpeedCalculation(0).magnitude);
        int sampleCount = currentMaxAngle % rangeSampleDegree == 0 ?
            (int) (currentMaxAngle / rangeSampleDegree) + 1 :
            (int) (currentMaxAngle / rangeSampleDegree) + 2;
        float minRange = minSpeed / 2;

        float[] maxRange = new float[sampleCount];
        for (int i = 0; i < sampleCount - 1; i++) {
            maxRange[i] = GetMaxSpeedByAngle((float) i * rangeSampleDegree, currentMaxAngle) / 2;
        }
        maxRange[sampleCount - 1] = GetMaxSpeedByAngle(currentMaxAngle, currentMaxAngle) / 2;

        int arraysize = (sampleCount * 4) - 1;
        Vector3[] points = new Vector3[arraysize];
        int currentPoint = 0;

        points[currentPoint] = GetRangePoint(minRange, -currentMaxAngle);
        currentPoint++;
        for (int i = sampleCount - 2; i >= 0; i--) {
            points[currentPoint] = GetRangePoint(minRange, -(rangeSampleDegree * i));
            currentPoint++;
        }
        for (int i = 1; i <= sampleCount - 2; i++) {
            points[currentPoint] = GetRangePoint(minRange, (rangeSampleDegree * i));
            currentPoint++;
        }
        points[currentPoint] = GetRangePoint(minRange, currentMaxAngle);
        currentPoint++;

        points[currentPoint] = GetRangePoint(maxRange[sampleCount - 1], currentMaxAngle);
        currentPoint++;
        for (int i = sampleCount - 2; i >= 0; i--) {
            points[currentPoint] = GetRangePoint(maxRange[i], (rangeSampleDegree * i));
            currentPoint++;
        }
        for (int i = 1; i <= sampleCount - 2; i++) {
            points[currentPoint] = GetRangePoint(maxRange[i], -(rangeSampleDegree * i));
            currentPoint++;
        }
        points[currentPoint] = GetRangePoint(maxRange[sampleCount - 1], -currentMaxAngle);
        currentPoint++;

        points[currentPoint] = points[0];

        rangeLine.positionCount = arraysize;
        rangeLine.SetPositions(points);
    }

    private void RemoveRangeLine() {
        rangeLine.positionCount = 0;
    }

    private Vector3 GetRangePoint(float range, float localAngle) {
        float worldAngle = (ship.transform.eulerAngles.z + localAngle) * Mathf.Deg2Rad;
        return speedPosition + (new Vector3(Mathf.Cos(worldAngle), Mathf.Sin(worldAngle), 0) * range);
    }

    private void ResetPoints() {
        startPosition = ship.transform.position;
        speedPosition = startPosition +
            (ship.transform.right * Mathf.Clamp(lastTurnSpeed, minSpeed, Mathf.Infinity) / 2);
        SetTargetPosition(speedPosition + (ship.transform.right * minSpeed / 2));
    }

    private void ResetMovementLine() {
        movementLine.positionCount = 0;
    }

    public void AllowToMoveByCalculation(bool value) {
        isAllowToMoveByCalcucation = value;
    }

    private float GetCurrentTimeRatio() {
        if (GameStateController.currentGameState != GameStateController.GameStates.Execute) {
            return 0;
        }
        else {
            return GameStateController.maxTime > 0 ?
                GameStateController.currentTime / GameStateController.maxTime : 1;
        }
    }

    public Vector3 GetCurrentSpeed() {
        return SpeedCalculation(GetCurrentTimeRatio());
    }

    public float GetCurrentSpeedRatio() {
        return Mathf.Clamp(GetCurrentSpeed().magnitude /
            maxSpeedByEnergy.Evaluate(ship.GetMaxEnergy(ShipEnergy.EnergyType.Maneuver)), 0, 1);
    }

    private void OnSelected(){
        CheckDrawRangeLine();
    }

    private void OnDeselected(){
        RemoveRangeLine();
    }
}