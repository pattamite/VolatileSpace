﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	//Singleton
	public static LevelManager instance;

	void Awake(){
		SetupSingleton();
	}

	void Start () {
		
	}
	
	void Update () {
		
	}

	private void SetupSingleton() {
		Debug.Assert(!instance, gameObject.name + ": there are more than 1");
		instance = this;
	}

	private void OnDestroy(){
		instance = null;
	}

	public static void LoadLevel(string name){
		instance.LoadLevelInstance(name);
	}

	public static void QuitGame(){
		instance.QuitGameInstance();
	}

	public void LoadLevelInstance(string name){
		SceneManager.LoadScene(name);
	}

	public void QuitGameInstance(){
		Application.Quit();
	}
	
	public void ToLobby()
	{
		if (NetworkController.instance != null && NetworkController.instance.isServer)
			LobbyManager.s_Singleton.ServerReturnToLobby();
	}
}

