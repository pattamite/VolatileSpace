﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAreaBound : MonoBehaviour {

	protected virtual void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position, transform.lossyScale);
	}

	protected virtual void OnTriggerEnter(Collider other){
        ShipHitbox shipHitbox = other.GetComponent<ShipHitbox>();
		if(shipHitbox && shipHitbox.ship){
			ShipEnter(shipHitbox.ship);
		}
    }

	protected virtual void OnTriggerExit(Collider other){
        ShipHitbox shipHitbox = other.GetComponent<ShipHitbox>();
		if(shipHitbox && shipHitbox.ship){
			ShipExit(shipHitbox.ship);
		}
    }

	protected virtual void ShipEnter(Ship ship){

	}

	protected virtual void ShipExit(Ship ship){

	}
}
