﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

public class GameController : MonoBehaviour {
	[Header("Player Info")]
	public int playerId;

	[Header("Ships")]
	public int shipsPerSide = 3;
	public Ship[] team1Ships;
	public Ship[] team2Ships;

	[Header("Debug")]
	public bool isDebugMode;
	public GameObject NetworkTester;
	public static bool isOfflineModeActivate { get; private set; }

	[Header("PlayField")]
	public ScrapFieldGenerator ScrapFieldGenerator;
	public int MinPieces = 3;
	public int MaxPieces = 13;
	public List<GameObject> SpawnZones = new List<GameObject>();

	//Singleton
	public static GameController instance;

	private void OnValidate() {
		if (team1Ships.Length != (shipsPerSide)) {
			System.Array.Resize<Ship>(ref team1Ships, shipsPerSide);
		}
		if (team2Ships.Length != (shipsPerSide)) {
			System.Array.Resize<Ship>(ref team2Ships, shipsPerSide);
		}
	}

	private void Awake() {
		SetupSingleton();
	}

	void Start() {
		CheckReferences();
	}

	void Update() {

	}

	private void SetupSingleton() {
		Debug.Assert(!instance, gameObject.name + ": there are more than 1");
		instance = this;
	}

	private void OnDestroy(){
		instance = null;
	}

	private void CheckReferences() {
		for (int i = 0; i < shipsPerSide; i++) {
			Debug.Assert(team1Ships[i], gameObject.name + ": playerShips[" + i + "] not found!");
			Debug.Assert(team2Ships[i], gameObject.name + ": enemyShips[" + i + "] not found!");
		}
		Debug.Assert(NetworkTester, gameObject.name + ": NetworkTester not found!");
	}

	public static void InitializePlayField() {
		instance.ScrapFieldGenerator.ExclusionZones.AddRange(instance.SpawnZones);
		instance.ScrapFieldGenerator.CreateField(UnityEngine.Random.Range(instance.MinPieces, instance.MaxPieces));
	}

	public static bool IsAllPlayerDead() {
		bool ans = true;
		foreach (Ship ship in GetShipsByPlayerId(instance.playerId)) {
			ans &= (!IsShipAlive(ship));
		}

		return ans;
	}

	public static bool IsAllEnemyDead() {
		bool ans = true;
		foreach (Ship ship in GetShipsByPlayerId(instance.playerId == 1 ? 2 : 1)) {
			ans &= (!IsShipAlive(ship));
		}

		return ans;
	}

	public static bool IsShipAlive(Ship ship){
		if(!ship){
			return false;
		}
		return ship.gameObject.activeSelf;

	}

	public static void CheckShipOutOfBound(){
		foreach(Ship ship in instance.team1Ships){
			if(ship){
				ship.OutOfBoundCheck();
			}
		}

		foreach(Ship ship in instance.team2Ships){
			if(ship){
				ship.OutOfBoundCheck();
			}
		}
	}

	public static bool IsPlayerId(int id) {
		return id == instance.playerId;
	}

	public static void SetPlayerId(int id) {
		instance.playerId = id;
	}

	public static Ship[] GetShipsByPlayerId(int id) {
		switch (id) {
			case 1:
				return instance.team1Ships;
			case 2:
				return instance.team2Ships;
			default:
				return null;
		}
	}

	public static void CheckOnline() {
		if (IsOfflineMode()) {
			Debug.Log("Start Offline Mode");
			for (int i = 0; i < instance.shipsPerSide; i++) {
				Destroy(instance.team1Ships[i].gameObject.GetComponent<NetworkTransform>());
				Destroy(instance.team1Ships[i].gameObject.GetComponent<NetworkIdentity>());
				instance.team1Ships[i].gameObject.SetActive(true);
				Destroy(instance.team2Ships[i].gameObject.GetComponent<NetworkTransform>());
				Destroy(instance.team2Ships[i].gameObject.GetComponent<NetworkIdentity>());
				instance.team2Ships[i].gameObject.SetActive(true);
			}
			isOfflineModeActivate = true;
		}
		else {
			Debug.Log("Start Online Mode");
			if (!IsServer()) {
				AllowShipToMoveByCalculation(false);
				AllowShipToAttackByCalculation(false);
			}
			isOfflineModeActivate = false;
		}
	}

	public static void AllowShipToMoveByCalculation(bool value) {
		for (int i = 0; i < instance.shipsPerSide; i++) {
			instance.team1Ships[i].AllowToMoveByCalculation(value);
			instance.team2Ships[i].AllowToMoveByCalculation(value);
		}
	}

	public static void AllowShipToAttackByCalculation(bool value) {
		for (int i = 0; i < instance.shipsPerSide; i++) {
			instance.team1Ships[i].AllowTAttackByCalculation(value);
			instance.team2Ships[i].AllowTAttackByCalculation(value);
		}
	}

	private static bool IsOfflineMode() {
		return (instance.isDebugMode && !instance.NetworkTester.activeSelf);
	}

	public static bool IsServer() {
		return (NetworkController.instance && NetworkController.instance.gameObject.activeSelf);
	}

	
	
}