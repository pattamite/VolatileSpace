﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PackageManager : MonoBehaviour {

	private static int energyTypeLength = System.Enum.GetValues(typeof(ShipEnergy.EnergyType)).Length;

	public static CommandPackage CreateCommandPackage(Ship[] ships) {
		CommandPackage package = new CommandPackage();
		package.shipCount = ships.Length;
		package.shipTargetPosition = new Vector3[ships.Length];
		package.energy = new float[ships.Length * energyTypeLength];
		package.shipStartPosition = new Vector3[ships.Length];
		package.shipSpeedPosition = new Vector3[ships.Length];
		package.shipTargetPosition = new Vector3[ships.Length];
		package.shipTargetNetId = new uint[ships.Length];

		for (int i = 0; i < ships.Length; i++) {
			if (ships[i]) {
				for (int j = 0; j < energyTypeLength; j++) {
					package.energy[(i * energyTypeLength) + j] = ships[i].GetRawCurrentEnergy((ShipEnergy.EnergyType) j);
				}
				package.shipStartPosition[i] = ships[i].shipMovement.startPosition;
				package.shipSpeedPosition[i] = ships[i].shipMovement.speedPosition;
				package.shipTargetPosition[i] = ships[i].shipMovement.targetPosition;
				if (ships[i].shipWeapon.shipTarget) {
					package.shipTargetNetId[i] = ships[i].shipWeapon.shipTarget.GetComponent<NetworkIdentity>().netId.Value;
				}
				else {
					package.shipTargetNetId[i] = 0;
				}

			}
			else {
				for (int j = 0; j < energyTypeLength; j++) {
					package.energy[(i * energyTypeLength) + j] = 0;
				}
				package.shipStartPosition[i] = new Vector3(0, 0, 0);
				package.shipSpeedPosition[i] = new Vector3(0, 0, 0);
				package.shipTargetPosition[i] = new Vector3(0, 0, 0);
				package.shipTargetNetId[i] = 0;
			}
		}

		return package;
	}

	public static void ReadCommandPackage(Ship[] ships, CommandPackage package) {
		if (ships.Length == package.shipCount) {
			for (int i = 0; i < ships.Length; i++) {
				if (ships[i]) {
					float[] shipEnergy = new float[energyTypeLength];
					for (int j = 0; j < energyTypeLength; j++) {
						shipEnergy[j] = package.energy[(i * energyTypeLength) + j];
					}
					ships[i].SetEnergy(shipEnergy);
					ships[i].SetStartAndSpeedPosition(package.shipStartPosition[i], package.shipSpeedPosition[i]);
					ships[i].SetTargetPosition(package.shipTargetPosition[i]);
					if (package.shipTargetNetId[i] != 0) {
						Ship target = GetObjectFromNetIdValue<Ship>(package.shipTargetNetId[i]);
						if (target) {
							ships[i].SetAttackTarget(target);
						}
					}
				}
			}
		}
		else {
			Debug.LogError("PackageManager ReadCommandPackage: ships.Length != package.shipCount ");
		}
	}

	public static T GetObjectFromNetIdValue<T>(uint netIdValue) {
		NetworkInstanceId netInstanceId = new NetworkInstanceId(netIdValue);
		NetworkIdentity foundNetworkIdentity = null;
		if (NetworkController.instance && NetworkController.instance.isServer) {
			NetworkServer.objects.TryGetValue(netInstanceId, out foundNetworkIdentity);
		}
		else {
			ClientScene.objects.TryGetValue(netInstanceId, out foundNetworkIdentity);
		}

		if (foundNetworkIdentity) {
			T foundObject = foundNetworkIdentity.GetComponent<T>();
			if (foundObject != null) {
				return foundObject;
			}
		}

		return default(T);
	}
}

public struct CommandPackage {
	public int shipCount;
	public float[] energy;
	public Vector3[] shipStartPosition;
	public Vector3[] shipSpeedPosition;
	public Vector3[] shipTargetPosition;
	public uint[] shipTargetNetId;
}