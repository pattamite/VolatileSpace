﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour , IObjectCreator
{

	public bool main = false;

	public bool workOnMultiplay;
	public bool workOnOffline;
	public bool workOnlyServer;
	

	public AreaSelectionInfo[] areaSelectionInfos;
	
	public List<Collider> exclusiveArea;

	private List<Section> sections;

	public static MapGenerator instance { get; private set; }

	[Serializable]
	public struct AreaSelectionInfo
	{
		public int maxRow;
		public int maxCol;
		public Collider mapArea;
	}
	
	[Serializable]
	public class Section
	{
		public Bounds bounds;
		public SectionState state;
	}
	
	public enum SectionState
	{
		VOID, EXCLUSION, USING
	}

	private void Awake()
	{
		if (main)
			instance = this;
	}

	private void Start()
	{
		Init();
	}

	private void Init ()
	{
		sections = new List<Section>();
		foreach (var info in areaSelectionInfos)
		{
			Vector3 center = info.mapArea.bounds.center;
			Vector3 maxSize = info.mapArea.bounds.size;
			float unitSizeX = maxSize.x / info.maxCol;
			float unitSizeY = maxSize.y / info.maxRow;
			Vector3 beginPos = center - maxSize / 2;
			beginPos.x += unitSizeX / 2;
			beginPos.y += unitSizeY / 2;
			for (int i = 0; i < info.maxCol; i++)
			{
				for (int j = 0; j < info.maxRow; j++)
				{
					Section section = new Section();
					section.bounds = new Bounds();
					section.bounds.center = beginPos + new Vector3(unitSizeX * i, unitSizeY * j, 0);
					section.bounds.size = new Vector3(unitSizeX, unitSizeY, maxSize.z);
					if (isIntersectWithExclusiveArea(section.bounds))
					{
						section.state = SectionState.EXCLUSION;
					}
					else
					{
						section.state = SectionState.VOID;
					}

					sections.Add(section);
				}
			}
		}
		Debug.Log("finish init map generator");
	}

	public void StartGenerate()
	{
		IObjectCreator[] objectCreators = this.GetComponentsInChildren<IObjectCreator>();
		foreach (var objectCreator in objectCreators)
		{
			if (objectCreator == this)
			{
				Debug.Log("Skip calling self method");
				continue;
			}

			bool work = !GameController.isOfflineModeActivate && objectCreator.WorkOnMultiplay();
			work &= !(objectCreator.WorkOnlyServer() ^ (NetworkController.instance != null &&
			        NetworkController.instance.isServer));
			work |= GameController.isOfflineModeActivate && objectCreator.WorkOnOffline();
			if (work)
			{
				objectCreator.GenerateObject(this);
			}
		}
	}

	public bool isIntersectWithExclusiveArea(Bounds bounds)
	{
		foreach (var area in exclusiveArea)
		{
			if (bounds.Intersects(area.bounds))
			{
				return true;
			}
		}

		return false;
	}

	
	public Section SelectSectionAndMark()
	{
		Section section = SelectSection();
		section.state = SectionState.USING;
		return section;
	}

	public Section SelectSection()
	{
		int triedCount = 0;
		int i;
		do
		{
			i = Random.Range(0, sections.Count);
			if (++triedCount > 100)
			{
				Debug.LogError("Try to get section over than 100 times");
				return sections[0];
			}
		} while (sections[i].state != SectionState.VOID);
		return sections[i];
	}

	public Section[] SelectPairSections()
	{
		int triedCount = 0;
		int i;
		do
		{
			i = Random.Range(0, sections.Count);
			if (++triedCount > 100)
			{
				Debug.LogError("Try to get section over than 100 times");
				return null;
			}
		} while (sections[i].state != SectionState.VOID || sections[sections.Count - i - 1].state != SectionState.VOID);
		return new Section[] {sections[i], sections[sections.Count - i - 1]};
	}

	public bool WorkOnlyServer()
	{
		return workOnlyServer;
	}

	public bool WorkOnMultiplay()
	{
		return workOnMultiplay;
	}

	public bool WorkOnOffline()
	{
		return workOnOffline;
	}

	public void GenerateObject(MapGenerator mapGenerator)
	{
		StartGenerate();
	}
}


public interface IObjectCreator
{
	bool WorkOnlyServer();
	bool WorkOnMultiplay();
	bool WorkOnOffline();
	void GenerateObject(MapGenerator mapGenerator);
}
