﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateObject {
	public bool isUseTimer { get; private set; }
	public float maxTime { get; protected set; }
	public float currentTime { get; protected set; }
	public GameStateController.GameStates stateName;
	public GameStateController.GameStates timeoutNextState;
	protected bool isStateFinished;

	public GameStateObject(float maxTime, GameStateController.GameStates timeoutNextState) {
		this.stateName = GameStateController.GameStates.None;
		this.timeoutNextState = timeoutNextState;
		this.isUseTimer = true;
		this.maxTime = maxTime;
		this.currentTime = 0;
		this.isStateFinished = false;
	}

	public GameStateObject() {
		this.stateName = GameStateController.GameStates.None;
		this.isUseTimer = false;
		this.maxTime = 0;
		this.currentTime = 0;
		this.isStateFinished = false;
	}

	public virtual void StartState() {

	}

	public virtual void UpdateState(float deltaTime) {
		if (isUseTimer) {
			UpdateTimer(deltaTime);
			if (isStateFinished) EndState(timeoutNextState);
		}
	}

	protected void EndState(GameStateController.GameStates nextState) {
		ExitState();
		GameStateController.ForceActivateState(nextState, false);
	}

	public virtual void ExitState() {

	}

	protected void UpdateTimer(float deltaTime) {
		currentTime += deltaTime;
		if (currentTime >= maxTime) {
			isStateFinished = true;
		}
	}

}

public class InitStateObject : GameStateObject {
	public InitStateObject(float maxTime, GameStateController.GameStates timeoutNextState) : base(maxTime, timeoutNextState) {
		stateName = GameStateController.GameStates.Init;
	}

	public override void StartState(){
//		GameController.InitializePlayField();
	}

	public override void UpdateState(float deltaTime) {
		//EndState(GameStateController.GameStates.NetworkInit);
		base.UpdateState(deltaTime);
	}

	public override void ExitState(){
		GameController.CheckOnline();
		CameraController.SetStartCamera();
		//GameController.InitializePlayField();
	}
}

public class NetworkInitStateObject : GameStateObject {
	public NetworkInitStateObject() : base() {
		stateName = GameStateController.GameStates.NetworkInit;
	}

	public override void UpdateState(float deltaTime)
	{
		if (GameController.isOfflineModeActivate)
		{
			EndState(GameStateController.GameStates.Planning);
		}
		else
		{
			if (NetworkController.instance != null)
			{
				NetworkController.instance.ChangeToPlanningStateIfReady();
			}
		}
		base.UpdateState(deltaTime);
	}
	
	public override void ExitState(){
		MapGenerator.instance.StartGenerate();
	}
}

public class PlanningStateObject : GameStateObject {
	public PlanningStateObject(float maxTime, GameStateController.GameStates timeoutNextState) : base(maxTime, timeoutNextState) {
		stateName = GameStateController.GameStates.Planning;
	}
}

public class NetworkStateObject : GameStateObject {
	public NetworkStateObject() : base() {
		stateName = GameStateController.GameStates.Network;
	}

	public override void UpdateState(float deltaTime) {
		if(GameController.isOfflineModeActivate) EndState(GameStateController.GameStates.Execute);
		base.UpdateState(deltaTime);
	}
}

public class ExecuteStateObject : GameStateObject {
	public ExecuteStateObject(float maxTime, GameStateController.GameStates timeoutNextState) : base(maxTime, timeoutNextState) {
		stateName = GameStateController.GameStates.Execute;
	}
}

public class EndingStateObject : GameStateObject {
	public EndingStateObject() : base() {
		stateName = GameStateController.GameStates.Ending;
	}

	public override void UpdateState(float deltaTime) {
		GameController.CheckShipOutOfBound();
		bool isAllPlayerDead = GameController.IsAllPlayerDead();
		bool isAllEnemyDead = GameController.IsAllEnemyDead();

		if (!isAllPlayerDead && !isAllEnemyDead) {
			EndState(GameStateController.GameStates.Planning);
		}
		else {
			if (isAllPlayerDead && isAllEnemyDead) {
				Debug.Log("Draw");
			}
			else if (isAllPlayerDead) {
				Debug.Log("You Lose!");
			}
			else if (isAllEnemyDead) {
				Debug.Log("You Win!");
			}

			EndState(GameStateController.GameStates.Done);
		}

		base.UpdateState(deltaTime);
	}
}

public class DoneStateObject : GameStateObject {
	public DoneStateObject() : base() {
		stateName = GameStateController.GameStates.Done;
	}
}