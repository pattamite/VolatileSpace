﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEventHandler : MonoBehaviour
{

	private bool isActive = true;

	public bool IsActive
	{
		get { return isActive; }
	}

	public void SetActive(bool active)
	{
		if (active)
		{
			isActive = true;
			Subscribe();
		}
		else
		{
			isActive = false;
			Unsubscribe();
		}
	}

	void Start()
	{
		if (isActive)
			Subscribe();
	}

	private void OnDestroy()
	{
		Unsubscribe();
	}

	private void Subscribe()
	{
		Ship.OnWeaponFireWithResult += OnWeaponFire;
		Ship.OnDead += OnShipDestroyed;
	}

	private void Unsubscribe()
	{
		Ship.OnWeaponFireWithResult -= OnWeaponFire;
		Ship.OnDead -= OnShipDestroyed;
	}
	
	private void OnWeaponFire(Ship ship, Ship targetShip, FireResult fireResult)
	{
		Collider targetCollider = targetShip.GetComponentInChildren<ShipHitbox>().GetComponent<Collider>();
		if (fireResult.afterHp <= 0)
		{
			EffectCreator.instance.CreateWeaponFire(ship.transform.position, targetCollider, targetShip.Dead);
		}
		else
		{
			EffectCreator.instance.CreateWeaponFire(ship.transform.position, targetCollider);
		}
	}

	private void OnShipDestroyed(Ship ship)
	{
		EffectCreator.instance.CreateShipExplode(ship.transform.position);
	}
}
	

