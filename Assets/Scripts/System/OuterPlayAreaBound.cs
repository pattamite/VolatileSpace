﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterPlayAreaBound : PlayAreaBound {

	protected override void OnDrawGizmos(){
		Gizmos.color = Color.red;
		base.OnDrawGizmos();
	}

	protected override void ShipEnter(Ship ship){
		print(ship + " Enter Outer Bound");
		ship.ResetOutOfBoundCount();
	}

	protected override void ShipExit(Ship ship){
		print(ship + " Exit Outer Bound");
		ship.OutOfBound();
	}
}
