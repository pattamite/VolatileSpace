﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	[Header("Panning")]
	public float panSpeedMouse = 10f;
	public float panSpeedTouch = 8f;
	public float panMinBoundX = -10f;
	public float panMaxBoundX = 10f;
	public float panMinBoundY = -10f;
	public float panMaxBoundY = 10f;
	[Range(1f, 5f)] public float panMaxMultiplayer = 2f;

	[Header("Zooming")]
	public float zoomSpeedMouse = 10f;
	public float zoomSpeedTouch = 0.02f;
	public float zoomMinBound = 5f;
	public float zoomMaxBound = 10f;

	[Header("Start Position")]
	public Vector2 defaultStartPosition;
	public Vector2 player1StartPosition;
	public Vector2 player2StartPosition;

	[Header("Ship Select Camera Speed")]
	public float cameraSpeed = 10f;

	//Panning Data
	private Vector3 lastPanPosition;

	//Zooming Data
	private bool isZooming;

	//Touch Data
	private Vector2[] lastTouchPositions;

	//Other Data
	private bool isAllowControl;
	private bool isMovingToShip;
	private Vector3 targetShipPosition;

	//Singleton
	public static CameraController instance;

	void Awake() {
		SetupSingleton();
		isAllowControl = true;
	}

	void Start() {

	}

	void Update() {
		if(isAllowControl){
			HandleControl();
		}
		if(isMovingToShip){
			MoveToShip(Time.deltaTime);
		}
	}

	private void MoveToShip(float deltaTime){
		Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, 
			targetShipPosition, cameraSpeed * deltaTime);
	}

	private void HandleControl(){
		if(Input.touchCount > 1){
		//if (Input.touchSupported) {
			TouchControl();
		}
		else {
			MouseControl();
		}
	}

	private void SetupSingleton() {
		Debug.Assert(!instance, gameObject.name + ": there are more than 1");
		instance = this;
	}

	private void OnDestroy(){
		instance = null;
	}

	private void TouchControl(){
		if(Input.touchCount == 2){
			if(Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(1).phase == TouchPhase.Began){
				lastTouchPositions = new Vector2[2] {Input.GetTouch(0).position, Input.GetTouch(1).position};
				lastPanPosition = (Input.GetTouch(0).position + Input.GetTouch(1).position) / 2;
			}
			else{
				PanCamera((Input.GetTouch(0).position + Input.GetTouch(1).position) / 2, true);

				float lastTouchDistance = (lastTouchPositions[0] - lastTouchPositions[1]).magnitude;
				float currentTouchDistance = (Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
				float deltaTouchDistance = currentTouchDistance - lastTouchDistance;
				ZoomCamera(deltaTouchDistance, true);

				lastTouchPositions = new Vector2[2] {Input.GetTouch(0).position, Input.GetTouch(1).position};
			}
		}
	}

	private void MouseControl() {
		if(Input.GetMouseButtonDown(1)){
			lastPanPosition = Input.mousePosition;
		}
		else if(Input.GetMouseButton(1)){
			PanCamera(Input.mousePosition, false);
		}

		float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
		ZoomCamera(mouseScroll, false);
	}

	private void PanCamera(Vector3 position, bool isTouch){
		instance.isMovingToShip = false;
		Vector3 diff = Camera.main.ScreenToViewportPoint(lastPanPosition - position);
		float panSpeed = (isTouch ? panSpeedTouch : panSpeedMouse) *
			Mathf.Lerp(1f, panMaxMultiplayer, Mathf.InverseLerp(zoomMinBound, zoomMaxBound, Camera.main.orthographicSize));
		Vector3 move = new Vector3(diff.x * panSpeed, diff.y * panSpeed, 0);

		Camera.main.transform.Translate(move, Space.World);

		Vector3 currentPosition = Camera.main.transform.position;
		currentPosition.x = Mathf.Clamp(currentPosition.x, panMinBoundX, panMaxBoundX);
		currentPosition.y = Mathf.Clamp(currentPosition.y, panMinBoundY, panMaxBoundY);
		Camera.main.transform.position = currentPosition;

		lastPanPosition = position;
	}

	private void ZoomCamera(float value, bool isTouch){
		if(value != 0){
			float zoomSpeed = isTouch ? zoomSpeedTouch : zoomSpeedMouse;

			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - (value * zoomSpeed), 
				zoomMinBound, zoomMaxBound);
		}
	}

	public static void SetStartCamera(){
		if(GameController.isOfflineModeActivate){
			SetCameraPosition(instance.defaultStartPosition);
		}
		else{
			switch(GameController.instance.playerId){
				case 1:
					SetCameraPosition(instance.player1StartPosition);
					break;
				case 2:
					SetCameraPosition(instance.player2StartPosition);
					break;
				default:
					SetCameraPosition(instance.defaultStartPosition);
					break;
			}
		}
	}

	private static void SetCameraPosition(Vector2 position){
		Camera.main.transform.position = new Vector3(position.x, position.y,
				Camera.main.transform.position.z);
	}

	public static void SetCameraPositionToShip(Ship ship){
		if(ship){
			instance.isMovingToShip = true;
			instance.targetShipPosition = new Vector3(ship.transform.position.x, 
				ship.transform.position.y, Camera.main.transform.position.z);
		}
	}
}