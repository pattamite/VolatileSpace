﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	[Header("Configs")]
	public int UiLayer = 5;
	[Range(1f, 200f)] public float raycastRange = 50f;

	//Singleton
	public static PlayerController instance;

	//Input Types
	public enum InputTypes { LeftUp, LeftDown, LeftClick, LeftDrag, LeftBeginDrag, LeftEndDrag }

	//Select State
	public enum SelectStates { NoSelect, SelectPlayer }
	public static SelectStates currentSelectState { get; private set; }

	//Ship Select
	public static bool isAnyShipSelected { get; private set; }
	public static Ship selectedShip { get; private set; }

	//Event
	public delegate void SelectAction(Ship ship);
	public static event SelectAction OnShipSelected;
	public static event SelectAction OnShipDeselected;
	public static event SelectAction OnBeginCommandDrag;
	public static event SelectAction OnEndCommandDrag;

	//Component
	public PositionKnob positionKnob;

	void Awake() {
		isAnyShipSelected = false;
		selectedShip = null;
		SetupSingleton();
	}

	void Start() {

	}

	void Update() {
		//temporary
		if (Input.GetKey(KeyCode.Escape)) LevelManager.QuitGame();
	}

	private void SetupSingleton() {
		Debug.Assert(!instance, gameObject.name + ": there are more than 1");
		instance = this;
	}

	private void OnDestroy(){
		instance = null;
	}

	public static void MainPanelUserInputHandling(InputTypes input, Vector3 position) {
		RaycastHit hit = new RaycastHit();
		Vector3 worldPosition = Camera.main.ScreenToWorldPoint(position);
		bool isHit = Physics.Raycast(Camera.main.ScreenPointToRay(position), out hit, instance.raycastRange, 1 << instance.UiLayer);

		switch (currentSelectState) {
			case SelectStates.NoSelect:
				NoSelectState(input, worldPosition, isHit, hit);
				break;
			case SelectStates.SelectPlayer:
				SelectPlayerState(input, worldPosition, isHit, hit);
				break;
		}
	}

	private static void NoSelectState(InputTypes input, Vector3 position, bool isHit, RaycastHit hit) {
		//todo
		Ship ship = null;
		HitRefernces(isHit, hit, ref ship);

		if (input == InputTypes.LeftClick && isHit && (GameController.IsPlayerId(ship.shipOwnerId) || GameController.isOfflineModeActivate)) {
			Select(ship);
			currentSelectState = SelectStates.SelectPlayer;
		}
	}

	private static void SelectPlayerState(InputTypes input, Vector3 position, bool isHit, RaycastHit hit) {
		//todo
		Ship ship = null;
		HitRefernces(isHit, hit, ref ship);

		//if (input == InputTypes.LeftClick && isHit && (GameController.IsPlayerId(ship.shipOwnerId) || GameController.isOfflineModeActivate)) {
		if (input == InputTypes.LeftClick && isHit && (GameController.IsPlayerId(ship.shipOwnerId))) {
			Select(ship);
			currentSelectState = SelectStates.SelectPlayer;
		}
		else if(input == InputTypes.LeftClick && isHit && !GameController.IsPlayerId(ship.shipOwnerId) && ship.IsSeen()){
			selectedShip.SetAttackTarget(ship);
			currentSelectState = SelectStates.SelectPlayer;
		}
		else if (input == InputTypes.LeftClick && !isHit) {
			Deselect(true);
			currentSelectState = SelectStates.NoSelect;
		}
	}

	private static void HitRefernces(bool isHit, RaycastHit hit, ref Ship ship) {
		if (isHit) {
			ship = hit.transform.GetComponent<Ship>();
		}
	}

	private static void Select(Ship ship) {
		if (isAnyShipSelected) Deselect();

		isAnyShipSelected = true;
		ship.Select();
		selectedShip = ship;

		if (OnShipSelected != null) {
			OnShipSelected(selectedShip);
		}

		
AudioHandler.CreateSound("UI_ShipSelect");
	}

	private static void Deselect(bool playsound = false) {
		if (OnShipDeselected != null) {
			OnShipDeselected(selectedShip);
		}

		isAnyShipSelected = false;
		if(selectedShip) selectedShip.Deselect();
		selectedShip = null;

		if(playsound){
			
AudioHandler.CreateSound("UI_ShipDeselect");
		}
	}

	private static void SetShipTargetPosition(Ship ship, Vector3 targetPosition, InputTypes input) {
		ship.SetTargetPosition(targetPosition);

		if (input == InputTypes.LeftBeginDrag && OnBeginCommandDrag != null) {
			print("Begin Command");
			OnBeginCommandDrag(ship);
		}
		else if (input == InputTypes.LeftEndDrag && OnBeginCommandDrag != null) {
			print("End Command");
			OnEndCommandDrag(ship);
		}

	}

	public static void ForceSelectShip(Ship ship){
		if(GameController.IsPlayerId(ship.shipOwnerId) || GameController.isOfflineModeActivate){
			Select(ship);
			currentSelectState = SelectStates.SelectPlayer;
		}
	}
}
