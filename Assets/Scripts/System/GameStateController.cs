﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour {
	//Singleton
	public static GameStateController instance;

	//GameState
	public enum GameStates { None, Init, NetworkInit, Planning, Network, Execute, Ending, Done }
	public static GameStates currentGameState {
		get {
			if (currentStateObject != null) return currentStateObject.stateName;
			else return GameStates.None;
		}
	}

	[Header("State Time")]
	[Range(0.1f, 3f)] public float initTime;
	[Range(10f, 120f)] public float planningTime;
	[Range(1f, 30f)] public float executeTime;
	public static float currentTime {
		get {
			if (currentStateObject != null) return currentStateObject.currentTime;
			else return 0f;
		}
	}
	public static float maxTime {
		get {
			if (currentStateObject != null) return currentStateObject.maxTime;
			else return 0f;
		}
	}
	private static GameStateObject currentStateObject;

	//Event
	public delegate void StateAction(GameStates state);
	public static event StateAction OnStateActivate;

	void Awake() {
		SetupSingleton();
	}

	void Start() {
		ForceActivateState(GameStates.Init, false);
	}

	void Update() {
		CheckGameState(Time.deltaTime);
	}

	private void SetupSingleton() {
		Debug.Assert(!instance, gameObject.name + ": there are more than 1");
		instance = this;
	}

	private void OnDestroy(){
		instance = null;
	}

	private static void CheckGameState(float deltaTime) {
		if (currentStateObject != null) {
			currentStateObject.UpdateState(deltaTime);
		}
	}

	private static void NotifyStateActivate() {
		Debug.Log(currentGameState  + " Start");
		if (OnStateActivate != null) {
			OnStateActivate(currentGameState);
		}
	}

	public static void ForceActivateState(GameStates state, bool useEndStateMethod){
		if(useEndStateMethod && currentStateObject != null){
			currentStateObject.ExitState();
		}
		CreateNewStateObject(state);
	}

	private static void CreateNewStateObject(GameStates state) {
		switch (state) {
			case GameStates.Init:
				currentStateObject = new InitStateObject(instance.initTime, GameStates.NetworkInit);
				break;
			case GameStates.NetworkInit:
				currentStateObject = new NetworkInitStateObject();
				break;
			case GameStates.Planning:
				currentStateObject = new PlanningStateObject(instance.planningTime, GameStates.Network);
				break;
			case GameStates.Network:
				currentStateObject = new NetworkStateObject();
				break;
			case GameStates.Execute:
				currentStateObject = new ExecuteStateObject(instance.executeTime, GameStates.Ending);
				break;
			case GameStates.Ending:
				currentStateObject = new EndingStateObject();
				break;
			case GameStates.Done:
				currentStateObject = new DoneStateObject();
				break;
			default:
				Debug.LogError("GameStateController: Unknown GameState");
				break;
		}

		NotifyStateActivate();
		currentStateObject.StartState();
	}

}