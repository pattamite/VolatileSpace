﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerPlayAreaBound : PlayAreaBound {

	protected override void OnDrawGizmos(){
		Gizmos.color = Color.green;
		base.OnDrawGizmos();
	}

	protected override void ShipEnter(Ship ship){
		print(ship + " Enter Inner Bound");
		ship.ResetAlmostOutOfBound();
	}

	protected override void ShipExit(Ship ship){
		print(ship + " Exit Inner Bound");
		ship.AlmostOutOfBound();
	}
}
