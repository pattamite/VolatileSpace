﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;

public class NetworkController : NetworkBehaviour
{
    
//    public NetworkEventHandler eventHandler;

    private bool[] ready;
    private bool[] receivedPlan;
    private CommandPackage[] buffer;

    [Range(10f, 120f)] public float planningTime;

    public float currentTime { get; private set; }

    public float maxTime { get; private set; }

    //Singleton
    public static NetworkController instance;
    
    private static int idCount = 0;

    
    [MethodImpl(MethodImplOptions.Synchronized)]
    public static int GetUniqueId()
    {
        idCount++;
        return idCount;
    }


    void Awake()
    {
        SetupSingleton();
        
        ready = new bool[2];
        receivedPlan = new bool[2];
        buffer = new CommandPackage[2];
    }

    void Start()
    {
        
        GameController.AllowShipToMoveByCalculation(isServer);
        GameController.AllowShipToAttackByCalculation(isServer);
    }
    
    void Update()
    {
        
        if (CheckEveryoneSentPlan())
            ExecutePlan();
    }
    

    private bool CheckEveryoneSentPlan()
    {
        foreach (var val in receivedPlan)
        {
            if (val == false)
                return false;
        }

        return true;
    }

    private void ExecutePlan()
    {
        for (int i = 0; i < buffer.Length; i++)
        {
            receivedPlan[i] = false;
        }

        PlayerRemote[] playerRemotes = (PlayerRemote[]) GameObject.FindObjectsOfType(typeof(PlayerRemote));
        foreach (var playerRemote in playerRemotes)
        {
            playerRemote.RpcExecute(buffer);
        }
    }

    private void SetupSingleton()
    {
        Debug.Assert(!instance, gameObject.name + ": there are more than 1");
        instance = this;
    }

    private void OnDestroy()
    {
        idCount = 0;
		instance = null;
	}


    public void ReceivedPackege(int playerId, CommandPackage package)
    {
        receivedPlan[playerId - 1] = true;
        buffer[playerId - 1] = package;
    }

    public void PlayerReady(int playerId)
    {
        ready[playerId - 1] = true;
        
    }

    public void ChangeToPlanningStateIfReady()
    {
        if (IsAllPlayerReady())
        {
            PlayerRemote[] playerRemotes = (PlayerRemote[]) GameObject.FindObjectsOfType(typeof(PlayerRemote));
            foreach (var playerRemote in playerRemotes)
            {
                playerRemote.RpcStartPlanning();
            }
        }
    }

    public bool IsAllPlayerReady()
    {
        return ready[0] && ready [1];
    }

    private bool CheckTimer()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= maxTime)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void ResetTimer(float maxTime)
    {
        currentTime = 0f;
        this.maxTime = maxTime;
    }
}