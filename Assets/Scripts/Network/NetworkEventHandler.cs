﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;

public class NetworkEventHandler : NetworkBehaviour
{

	public static NetworkEventHandler instance{ get; private set; }

	private void Awake()
	{
		instance = this;
	}

	public override void OnStartServer () {
		Debug.Log("Setting NetworkEventHandler");
		Ship.OnWeaponFireWithResult += OnWeaponFire;
		Ship.OnDead += OnShipDestroyed;
	}

	void OnDestroy(){
		Ship.OnWeaponFireWithResult -= OnWeaponFire;
		Ship.OnDead -= OnShipDestroyed;
	}
	
	private void OnWeaponFire(Ship ship, Ship targetShip, FireResult fireResult)
	{
		NetworkIdentity shipIdentity = ship.GetComponent<NetworkIdentity>();
		NetworkIdentity targetShipIdentity = targetShip.GetComponent<NetworkIdentity>();
		RpcWeaponFire(shipIdentity, targetShipIdentity, fireResult);
	}
	
	private void OnShipDestroyed(Ship ship)
	{
		NetworkIdentity shipIdentity = ship.GetComponent<NetworkIdentity>();
		RpcShipDestroyed(shipIdentity);
	}

	[ClientRpc]
	public void RpcWeaponFire(NetworkIdentity shipIdentity, NetworkIdentity targetShipIdentity, FireResult fireResult) 
	{
		// create fire effect
		Debug.Log("Weapon Fire from : "  + shipIdentity.gameObject.name + ", to : " + targetShipIdentity.gameObject.name);
		if (!isServer)
		{
			Ship.WeaponFireEventWithResult(shipIdentity.GetComponent<Ship>(), targetShipIdentity.GetComponent<Ship>(),
				fireResult);
		}
	}
	
	[ClientRpc]
	public void RpcShipDestroyed(NetworkIdentity shipIdentity) 
	{
		Debug.Log("Ship Destroyed :" + shipIdentity.gameObject.name );
		if (!isServer)
		{
			shipIdentity.GetComponent<Ship>().Dead();
		}
	}

	public void ChangeShipModelNetwork(Ship[] ships, int index)
	{
		Debug.Log("ChangeShipModelNetwork");
		foreach (var ship in ships)
		{
			RpcChangeShipModel(ship.GetComponent<NetworkIdentity>(), index);
		}
	}

	[ClientRpc]
	public void RpcChangeShipModel(NetworkIdentity shipIdentities, int modelIndex)
	{
		ShipModelLoader.instance.ReplaceShipModel(shipIdentities.GetComponent<Ship>(), modelIndex);
	}
 	
}
