﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class VolatileDiscovery : NetworkDiscovery
{

	private UIListController listController;
	
	private HashSet<String> serverAddressSet = new HashSet<string>();


	void Start()
	{
		if (!listController)
		{
			listController = GetComponent<UIListController>();
		}
	}

	public void StartServer()
	{
		this.Initialize();
		this.StartAsServer();
	}

	public void StartListening()
	{
		this.Initialize();
		this.StartAsClient();
		serverAddressSet = new HashSet<string>();
	}
	
	public override void OnReceivedBroadcast(string fromAddress, string data)
	{
		if (!serverAddressSet.Contains(fromAddress))
		{
			listController.CreateOne(obj =>
			{
				obj.GetComponentInChildren<Text>().text = fromAddress + "\n" + data;
				obj.GetComponent<Button>().onClick.AddListener(delegate {JoinServer(fromAddress);});
			});
			serverAddressSet.Add(fromAddress);
		}
	}

	public void Reset()
	{
		serverAddressSet = new HashSet<string>();
		listController.RemoveAll();
	}

	public void JoinServer(String address)
	{
		Debug.Log("Join address:" + address);
	}
	
	
}
