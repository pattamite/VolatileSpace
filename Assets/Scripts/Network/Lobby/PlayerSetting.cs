﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetting : MonoBehaviour
{
	private LobbyPlayer player;
	
	public static PlayerSetting instance { get; private set; }

	void Awake()
	{
		instance = this;
		gameObject.SetActive(false);
	}

	void Start()
	{
	}

	public void Open(LobbyPlayer player)
	{
		this.player = player;
		ShipPreviewController.instance.Show(player.modelIndex);
		gameObject.SetActive(true);
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}

	public void OnModelChange(LobbyPlayer player)
	{
		if (player == this.player)
		{
			ShipPreviewController.instance.Show(player.modelIndex);
		}
	}

	public void ChangeModelForward()
	{
		if (player.isLocalPlayer)	
			player.CmdChangeModelForward();
	}
	
	
	public void ChangeModelBackward()
	{
		if (player.isLocalPlayer)	
			player.CmdChangeModelBackward();
	}

}

[Serializable]
public class PlayerParameter
{
	public int modelIndex = -1;
}
