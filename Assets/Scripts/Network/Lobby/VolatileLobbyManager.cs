﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class VolatileLobbyManager : NetworkLobbyManager {

	// Use this for initialization
	void Start ()
	{
		this.StartMatchMaker();
//		ListMatches();
	}

	public void ListMatches()
	{
		this.matchMaker.ListMatches(0, 10, "", true, 0, 0, OnMatchList);
	}

	public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
	{
		base.OnMatchList(success, extendedInfo, matchList);
	}
}
