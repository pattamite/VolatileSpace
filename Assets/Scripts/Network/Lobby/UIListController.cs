﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIListController : MonoBehaviour
{

	public RectTransform contentList;
	public GameObject itemPrefab;
	public List<GameObject> itemList;

	public delegate void InitializeMethod(GameObject item);

	public void CreateOne(InitializeMethod initializeMethod)
	{
		GameObject o = Instantiate(itemPrefab, contentList);
		initializeMethod(o);
		itemList.Add(o);
	}

	public void RemoveAll()
	{
		foreach (var item in itemList)
		{
			Destroy(item);
		}
	}

}
