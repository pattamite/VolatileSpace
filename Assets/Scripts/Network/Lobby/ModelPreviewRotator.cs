﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelPreviewRotator : MonoBehaviour
{

	public float speed = 0.5f;
	public Vector3 axis = Vector3.up;
	
	// Use this for initialization
	void Start ()
	{
		axis = axis.normalized;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(speed * axis * Time.deltaTime);
	}
}
