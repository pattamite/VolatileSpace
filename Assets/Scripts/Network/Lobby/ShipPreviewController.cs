﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPreviewController : MonoBehaviour
{

	public Transform modelParent;
	public ShipModelLoader modelLoader;
	
	public static ShipPreviewController instance;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		modelLoader = GetComponent<ShipModelLoader>();
	}

	public void Show(int index)
	{
		modelLoader.ReplaceModelAndRemoveTrail(modelParent, index);
		modelParent.gameObject.SetActive(true);
	}

	public void Hide()
	{
		modelParent.gameObject.SetActive(false);
	}
}
