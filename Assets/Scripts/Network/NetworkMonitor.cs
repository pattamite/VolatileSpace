﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkMonitor : MonoBehaviour
{

	public float bytesRate = 0;
	public float peekBytesRate = 0;
	
	public int[] buffer;
	private int bufferPtr;
	private float interval = 0.25f;
	private float calculateBackTime = 3f;
	private float lastCollectTime;

	private float peekResetTime = 10f;
	
	// Use this for initialization
	void Start ()
	{
		peekBytesRate = 0;
		buffer = new int[ (int) (calculateBackTime / interval)];
		for (int i = 0; i < buffer.Length; i++)
		{
			buffer[i] = Int32.MaxValue;
		}

		StartCoroutine(resetPeek());
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Debug.Log("Outgoing Bytes Count:" + NetworkTransport.GetOutgoingFullBytesCount());
		if (lastCollectTime < Time.time)
		{
			buffer[bufferPtr] = NetworkTransport.GetOutgoingFullBytesCount();
			int afterByteCount = buffer[bufferPtr];
			int beforeByteCount = buffer[(bufferPtr + 1) % buffer.Length];
			bytesRate = (afterByteCount - beforeByteCount) / calculateBackTime;
			bufferPtr = (bufferPtr + 1) % buffer.Length;
			
//			Debug.Log("Bytes Rate:" + bytesRate);
			if (bytesRate > peekBytesRate)
			{
				peekBytesRate = bytesRate;
//				Debug.Log("Peek Bytes Rate:" + peekBytesRate);
			}
		}
	}

	IEnumerator resetPeek()
	{
		while (true)
		{
			yield return new WaitForSeconds(peekResetTime);
			peekBytesRate = 0;
		}
	}
	
	void OnGUI()
	{
		GUI.Label(new Rect(200, 10, 200, 20), "Bytes Rate: " + bytesRate);
		GUI.Label(new Rect(200, 30, 200, 20), "Peek Bytes Rate:" + peekBytesRate);
	}
}
