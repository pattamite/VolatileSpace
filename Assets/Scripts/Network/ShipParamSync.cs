﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ShipParamSync : NetworkBehaviour
{

	
	public Ship parentShip;
	
	[SyncVar(hook = "OnChangeCurrentHealth")]
	public float currentHealth;
	[SyncVar(hook = "OnChangeCurrentShield")]
	public float currentShield;



	// Update is called once per frame
	void Update()
	{
		if (isServer)
		{
			currentHealth = parentShip.shipHealth.currentHealth;
			currentShield = parentShip.shipShield.currentShield;
		}
	}

	public void OnChangeCurrentHealth(float health)
	{
		currentHealth = health;
		parentShip.shipHealth.CurrentHealth = health;
	}

	public void OnChangeCurrentShield(float shield)
	{
		currentShield = shield;
		parentShip.shipShield.CurrentShield = shield;
	}


	
}
