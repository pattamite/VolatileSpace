﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class PlayerRemote : NetworkBehaviour
{
	[SyncVar]
	public int id;
	[SyncVar]
	public int modelIndex = -1;

	public override void OnStartServer()
	{
		id = NetworkController.GetUniqueId();
		
//		NetworkEventHandler.instance.ChangeShipModelNetwork(GameController.GetShipsByPlayerId(id), modelIndex);
	}

	void Start()
	{
		if (this.isLocalPlayer)
		{
			GameController.SetPlayerId(id);
			GameStateController.OnStateActivate += OnStateActivate;
		}
		
		ShipModelLoader.instance.ReplaceShipModel(GameController.GetShipsByPlayerId(id), modelIndex);
	}

	private void OnDestroy()
	{
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	public void OnStateActivate(GameStateController.GameStates state)
	{
		if (state == GameStateController.GameStates.Network)
		{
			SendPlan();
		}
		else if (state == GameStateController.GameStates.NetworkInit)
		{
			CmdReady();
		}
	}

	public void SendPlan()
	{
		Ship[] ships = GameController.GetShipsByPlayerId(id);
		Debug.Log("CmdSendPlan with " + ships.Length + " ships");
		CmdSendPlan(PackageManager.CreateCommandPackage(ships));
//		short msgType = (short) (NetworkController.SENDPLAN_START + id - 1);
//		CommandHolderMessage message = new CommandHolderMessage(PackageManager.CreateCommandPackage(ships));
//		NetworkManager.singleton.client.Send(msgType, message);
	}
	
	
	[Command]
	public void CmdSendPlan(CommandPackage package)
	{
		NetworkController.instance.ReceivedPackege(id, package);
	}


	[ClientRpc]
	public void RpcExecute(CommandPackage[] packages)
	{
		if (!isLocalPlayer) 
			return;
		for (int i = 0; i < packages.Length; i++)
		{
			Ship[] ships = GameController.GetShipsByPlayerId(i + 1);
			PackageManager.ReadCommandPackage(ships, packages[i]);
		}
		
		GameStateController.ForceActivateState(GameStateController.GameStates.Execute, false);
	}

	[Command]
	public void CmdReady()
	{
		NetworkController.instance.PlayerReady(id);
	}

	[ClientRpc]
	public void RpcStartPlanning()
	{
		GameStateController.ForceActivateState(GameStateController.GameStates.Planning, true);
	}

}
