﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueControl : MonoBehaviour {

  public float valueMin = 0.0f;
  public float valueMax = 1.0f;

  public float changeValue = 0.05f;

  public float currentValue = 1.0f;
  public float defaultValue = 1.0f;

  public bool toPercent = true;

  public string title = "Enter title here";
  public string handlerID = "null";

  public delegate void OnValueChanged(string handler, float value);
  public static event OnValueChanged OnValueChangeEvent;
  // Use this for initialization
  void Start () {
    // ** ----- Show config title
    Transform titleText = this.transform.Find("TitleText");
    titleText.GetComponent<Text>().text = this.title;
    // ** ----- Show initial value
    currentValue = PlayerPrefs.GetFloat(handlerID, defaultValue);
    UpdateValue();
  }

  // Update is called once per frame
  void Update () {

  }

  public void MinusPress() {
    this.currentValue = Mathf.Clamp(this.currentValue - changeValue, valueMin, valueMax);
    UpdateValue();
  }

  public void PlusPress() {
    this.currentValue = Mathf.Clamp(this.currentValue + changeValue, valueMin, valueMax);
    UpdateValue();
  }

  public void UpdateValue() {
    Transform valueText = this.transform.Find("ValueText");
    if (this.toPercent == false) {
      valueText.GetComponent<Text>().text = this.currentValue.ToString("#.00");
    }
    else {
      valueText.GetComponent<Text>().text = (Mathf.Round(this.currentValue * 100)).ToString() + "%";
    }
    if (OnValueChangeEvent != null) {
      OnValueChangeEvent(handlerID, currentValue);
    }
  }

}
