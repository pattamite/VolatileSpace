﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour {

  public GameObject otherPanel;

  // Use this for initialization
  void Start () {
    ValueControl.OnValueChangeEvent += ValueChange;
  }

  // Update is called once per frame
  void Update () {

  }

  void OnDestroy(){
    ValueControl.OnValueChangeEvent -= ValueChange;
  }

  public void ToggleVisible() {
    if (this.gameObject.activeSelf) {
      this.gameObject.SetActive(false);
    }
    else {
      this.gameObject.SetActive(true);
    }
    if (otherPanel != null) {
      otherPanel.gameObject.SetActive(!this.gameObject.activeSelf);
    }
  }

  void ValueChange(string handler, float value) {
    if (handler != "null") {
      value = (float)((int)(Mathf.Floor(value * 100.0f)) / 100.0);
      PlayerPrefs.SetFloat(handler, value);
      PlayerPrefs.Save();
      // ** ----- Sub-sections
      if (handler[0] == 'a') {
        VolumeController.RefreshVolume();
        //Debug.Log("save_val = " + value);
      }
    }
    else {
      Debug.Log("Please set the handler value in the property panel");
    }
  }
}
