﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningText : MonoBehaviour {
	private static WarningText instance;

	[Header("Component")]
	public Text[] warningText;

	[Header("Time")]
	[Range(1f, 10f)] public float messageTime = 5f;
	[Range(0.1f, 1f)] public float refreshTime = 0.5f;

	private static Queue<string> waitingMessageList = new Queue<string>();
	private static LinkedList<string> activeMessageList = new LinkedList<string>();
	private static LinkedList<float> activeMessageStopTime = new LinkedList<float>();

	// Use this for initialization
	void Start() {
		instance = this;
		foreach (Text text in warningText) {
			text.text = "";
		}

		StartCoroutine("UpdateMessage");
	}

	// Update is called once per frame
	void Update() { }

	public static void AddInnerOutOfBoundMessage(Ship ship) {
		if (CheckShip(ship)) {
			AddMessage(ship.shipName + " is almost out of combat area.");
		}
	}

	public static void AddOuterOutOfBoundMessage(Ship ship, int turnUntilDead) {
		if (CheckShip(ship) && turnUntilDead == 1) {
			AddMessage(ship.shipName + " is out of combat area.\r\nSignal lost in 1 turn.");
		}
		else if (CheckShip(ship)) {
			AddMessage(ship.shipName + " is out of combat area.\r\nSignal lost in " + turnUntilDead.ToString() + " turns.");
		}
	}

	private static void AddMessage(string message) {
		waitingMessageList.Enqueue(message);
	}

	private static bool CheckShip(Ship ship) {
		return ship && GameController.IsPlayerId(ship.shipOwnerId);
	}

	private IEnumerator UpdateMessage() {
		bool changeFlag;
		LinkedListNode<string> currentNode;
		int currentCount;
		WaitForSeconds wait = new WaitForSeconds(refreshTime);

		while (true) {
			yield return wait;
			changeFlag = false;

			while (activeMessageList.Count > 0 && activeMessageStopTime.Last.Value <= Time.time) {
				activeMessageList.RemoveLast();
				activeMessageStopTime.RemoveLast();
				changeFlag = true;
			}

			while (activeMessageList.Count < warningText.Length && waitingMessageList.Count > 0) {
				activeMessageList.AddFirst(waitingMessageList.Dequeue());
				activeMessageStopTime.AddFirst(Time.time + messageTime);
				changeFlag = true;
			}

			if (changeFlag) {
				currentNode = activeMessageList.First;
				currentCount = 0;

				while (currentNode != null) {
					warningText[currentCount].text = currentNode.Value;
					currentNode = currentNode.Next;
					currentCount++;
				}

				for (; currentCount < warningText.Length; currentCount++) {
					warningText[currentCount].text = "";
				}
			}
		}
	}
}