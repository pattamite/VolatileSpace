﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatePanel : MonoBehaviour {
	public Text stateText;
	public Text timerText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		stateText.text = GetStateName(GameStateController.currentGameState);
		timerText.text = GateTimer(GameStateController.currentGameState);
	}

	private string GetStateName(GameStateController.GameStates state){
		switch(state){
			case GameStateController.GameStates.Planning:
				return "Planning";
			case GameStateController.GameStates.Execute:
				return "Executing";
			case GameStateController.GameStates.Network:
				return "Waiting";
			default:
				return "";
		}
	}

	private string GateTimer(GameStateController.GameStates state){
		if(state == GameStateController.GameStates.Planning || state == GameStateController.GameStates.Execute){
			return (GameStateController.maxTime - GameStateController.currentTime).ToString("F2");
		}
		else{
			return "";
		}
	}
}
