﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EnergyControlPanel : MonoBehaviour, IDragHandler {
    public bool interactable = false;

    public Canvas canvas;
    public RectTransform background;
    public RectTransform knob;

    public float offset = 30f;
    //	public float maxEnergy = 10f;

    //	public Text shieldText;
    //	public Text movementText;
    //	public Text weaponText;

    public float shieldEnergyRaw;
    public float movementEnergyRaw;
    public float weaponEnergyRaw;

    private Vector3 topPos;
    private Vector3 rightPos;
    private Vector3 leftPos;
    private float allArea;

    private Ship currentShip;

    // Use this for initialization
    void Start() {
        Vector3 backgroundPos = Camera.main.WorldToScreenPoint(background.position);

        topPos = backgroundPos + background.rect.yMax * canvas.scaleFactor * Vector3.up;
        topPos.z = 0;
        rightPos = backgroundPos + new Vector3(background.rect.xMax, background.rect.yMin, 0) * canvas.scaleFactor;
        rightPos.z = 0;
        leftPos = backgroundPos + new Vector3(background.rect.xMin, background.rect.yMin, 0) * canvas.scaleFactor;
        leftPos.z = 0;
        allArea = TriangleArea(topPos, rightPos, leftPos);

        CalculateEnergy();

        PlayerController.OnShipSelected += ShipSelected;
        PlayerController.OnShipDeselected += ShipDeselected;
        GameStateController.OnStateActivate += OnStateActivate;
        Ship.OnDead += OnShipDead;

        ShipDeselected(null);
    }

    void OnDestroy() {
        PlayerController.OnShipSelected -= ShipSelected;
        PlayerController.OnShipDeselected -= ShipDeselected;
        GameStateController.OnStateActivate -= OnStateActivate;
        Ship.OnDead -= OnShipDead;
    }

    public void OnDrag(PointerEventData eventData) {
        if (!interactable)
            return;

        Vector3 pos = eventData.position;
        Vector3 backgroundPos = Camera.main.WorldToScreenPoint(background.position);
        if (!IsInsideArea(pos + (pos - backgroundPos).normalized * offset)) {
            Vector3 edge0 = leftPos - topPos;
            Vector3 edge1 = rightPos - topPos;
            Vector3 v0 = topPos - pos;

            float a = Vector3.Dot(edge0, edge0);
            float b = Vector3.Dot(edge0, edge1);
            float c = Vector3.Dot(edge1, edge1);
            float d = Vector3.Dot(edge0, v0);
            float e = Vector3.Dot(edge1, v0);

            float det = a * c - b * b;
            float s = b * e - c * d;
            float t = b * d - a * e;

            if (s + t < det) {
                if (s < 0) {
                    if (t < 0) {
                        if (d < 0) {
                            s = Mathf.Clamp01(-d / a);
                            t = 0;
                        }
                        else {
                            s = 0;
                            t = Mathf.Clamp01(-e / c);
                        }
                    }
                    else {
                        s = 0;
                        t = Mathf.Clamp01(-e / c);
                    }
                }
                else if (t < 0) {
                    s = Mathf.Clamp01(-d / a);
                    t = 0;
                }
                else {
                    float invDet = 1 / det;
                    s *= invDet;
                    t *= invDet;
                }
            }
            else {
                if (s < 0) {
                    float tmp0 = b + d;
                    float tmp1 = c + e;
                    if (tmp1 > tmp0) {
                        float numer = tmp1 - tmp0;
                        float denom = a - (2 * b) + c;
                        s = Mathf.Clamp01(numer / denom);
                        t = 1 - s;
                    }
                    else {
                        t = Mathf.Clamp01(-e / c);
                        s = 0;
                    }
                }
                else if (t < 0) {
                    if (a + d > b + e) {
                        float numer = c + e - b - d;
                        float denom = a - (2 * b) + c;
                        s = Mathf.Clamp01(numer / denom);
                        t = 1 - s;
                    }
                    else {
                        s = 1;
                        t = 0;
                    }
                }
                else {
                    float numer = c + e - b - d;
                    float denom = a - (2 * b) + c;
                    s = Mathf.Clamp01(numer / denom);
                    t = 1 - s;
                }
            }
            pos = topPos + (s * edge0) + (t * edge1);
        }

        Vector3 prevKnobPos = Camera.main.WorldToScreenPoint(knob.position);
        prevKnobPos.Set(pos.x, pos.y, prevKnobPos.z);
        knob.position = Camera.main.ScreenToWorldPoint(prevKnobPos);

        CalculateEnergy();
        SetEnergyToShip();
    }

    public void CalculateEnergy() {
        Vector3 pos = Camera.main.WorldToScreenPoint(knob.position);
        
        shieldEnergyRaw = TriangleArea(pos, leftPos, rightPos) / allArea;
        movementEnergyRaw = TriangleArea(pos, topPos, rightPos) / allArea;
        weaponEnergyRaw = TriangleArea(pos, leftPos, topPos) / allArea;

        //		shieldEnergy = Mathf.RoundToInt(TriangleArea(pos, leftPos, rightPos) / allArea * maxEnergy);
        //		movementEnergy = Mathf.RoundToInt(TriangleArea(pos, topPos, rightPos) / allArea * maxEnergy);
        //		weaponEnergy = Mathf.RoundToInt(TriangleArea(pos, leftPos, topPos) / allArea * maxEnergy);
    }

    private void SetEnergyToShip() {
        if (!currentShip)
            return;

        float[] value = new float[System.Enum.GetValues(typeof(ShipEnergy.EnergyType)).Length];
        value[(int) ShipEnergy.EnergyType.Shield] = shieldEnergyRaw * currentShip.shipEnergy.maxShield;
        value[(int) ShipEnergy.EnergyType.Weapon] = weaponEnergyRaw * currentShip.shipEnergy.maxWeapon;
        value[(int) ShipEnergy.EnergyType.Maneuver] = movementEnergyRaw * currentShip.shipEnergy.maxManeuver;

        //        print(value[0] + " : " + value[1] + " : " + value[2]);
        currentShip.SetEnergy(value);
    }

    public bool IsInsideArea(Vector3 pos) {
        bool b1, b2, b3;

        b1 = Sign(pos, topPos, rightPos) < 0.0f;
        b2 = Sign(pos, rightPos, leftPos) < 0.0f;
        b3 = Sign(pos, leftPos, topPos) < 0.0f;

        return ((b1 == b2) && (b2 == b3));
    }

    public void SetEnergy() {
        if (!currentShip)
            return;
        float shieldRaw = currentShip.GetRawCurrentEnergy(ShipEnergy.EnergyType.Shield) / currentShip.shipEnergy.maxShield;
        float movementRaw = currentShip.GetRawCurrentEnergy(ShipEnergy.EnergyType.Maneuver) /
            currentShip.shipEnergy.maxManeuver;
        SetEnergy(shieldRaw, movementRaw);
    }

    public void SetEnergy(float shieldRaw, float movementRaw) {
        if (shieldRaw + movementRaw > 1 && shieldRaw < 0 && movementRaw < 0)
            throw new Exception("Sum of shield and movement must not more than 1 and each values must be positive");
        this.shieldEnergyRaw = shieldRaw;
        this.movementEnergyRaw = movementRaw;
        this.weaponEnergyRaw = 1 - shieldEnergyRaw - movementEnergyRaw;

        float d1 = 2 * shieldEnergyRaw * allArea / Vector3.Distance(leftPos, rightPos);
        float d2 = 2 * movementEnergyRaw * allArea / Vector3.Distance(topPos, rightPos);

        float theta = Mathf.Acos((rightPos.x - topPos.x) / Vector3.Distance(topPos, rightPos));
        float m = (topPos.y - rightPos.y) / (topPos.x - rightPos.x);
        float k = leftPos.y - m * (rightPos.x - d2 / Mathf.Sin(theta));
        float y = leftPos.y + d1;
        float x = (y - k) / m;

        Vector3 prevKnobPos = Camera.main.WorldToScreenPoint(knob.position);
        prevKnobPos.Set(x, y, prevKnobPos.z);
        //        print(prevKnobPos);
        knob.position = Camera.main.ScreenToWorldPoint(prevKnobPos);
    }

    public void ShipSelected(Ship ship) {
        gameObject.SetActive(true);
        currentShip = ship;
        SetEnergy();
        interactable = ((GameController.IsPlayerId(ship.shipOwnerId) || GameController.isOfflineModeActivate) &&
            (GameStateController.currentGameState == GameStateController.GameStates.Planning));
    }

    public void ShipDeselected(Ship ship) {
        gameObject.SetActive(false);
    }

    private void OnStateActivate(GameStateController.GameStates state) {
        if (state == GameStateController.GameStates.Planning) {
            if (currentShip && (GameController.IsPlayerId(currentShip.shipOwnerId) ||
                    GameController.isOfflineModeActivate)) {
                interactable = true;
            }
        }
        else {
            interactable = false;
        }
    }

    private void OnShipDead(Ship ship) {
        if (ship == currentShip) {
            gameObject.SetActive(false);
        }
    }

    static float Sign(Vector3 p1, Vector3 p2, Vector3 p3) {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    }

    static float TriangleArea(Vector3 p1, Vector3 p2, Vector3 p3) {
        return Mathf.Abs(p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x * (p1.y - p2.y)) / 2f;
    }

}