﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PositionKnob : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	private bool isClicked = false;
	private bool isEnable = false;
	private bool isInteractable = false;
	private RectTransform rectTransform;
	private Canvas canvas;
	private Ship currentShip;

	// Use this for initialization
	void Start () {
		rectTransform = GetComponent<RectTransform>();
		canvas = transform.parent.GetComponent<Canvas>();
		PlayerController.OnShipSelected += Enable;
		PlayerController.OnShipDeselected += Disable;
		GameStateController.OnStateActivate += OnStateActivate;
		Disable(null);
	}
	
	// Update is called once per frame
	void Update () {
		if(!isClicked && isEnable && currentShip){
			UpdatePosition();
		}
		
	}

	void UpdatePosition(){
        transform.position = new Vector3(currentShip.shipMovement.targetPosition.x,
			currentShip.shipMovement.targetPosition.y, transform.position.z);
	}

	void OnDestroy(){
		PlayerController.OnShipSelected -= Enable;
		PlayerController.OnShipDeselected -= Disable;
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	public void OnDrag(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			Vector2 pos;
         	RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform,
				eventData.position, canvas.worldCamera, out pos);
         	transform.position = canvas.transform.TransformPoint(pos);

			 if(currentShip){
				 currentShip.SetTargetPosition(transform.position);
				 transform.position = new Vector3(currentShip.shipMovement.targetPosition.x,
				 	currentShip.shipMovement.targetPosition.y, transform.position.z);
			 }
		}
	}

	public void OnPointerDown(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			isClicked = true;
		}
	}

	public void OnPointerUp(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			isClicked = false;
		}
	}

	private bool InputCondition(PointerEventData eventData) {
		return (eventData.button == PointerEventData.InputButton.Left ||
			(Input.touchSupported && Input.touchCount == 1)) && isInteractable;
	}

	public void Enable(Ship ship){
		if(ship && GameController.IsShipAlive(ship)){
			currentShip = ship;
			if(isInteractable){
				isClicked = false;
				isEnable = true;
			}
		}
	}

	public void Disable(Ship ship){
		currentShip = null;
		transform.position = new Vector3(-1000, -1000, transform.position.z);
		isEnable = false;
	}

	private void OnStateActivate(GameStateController.GameStates state){
		switch(state){
			case GameStateController.GameStates.Planning:
				isInteractable = true;
				Enable(PlayerController.selectedShip);
				break;
			default:
				isInteractable = false;
				Disable(null);
				break;
		}
	}
}
