﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioSlider : MonoBehaviour {

	public string key;
	private Slider slider;


	// Use this for initialization
	void Start () {
		slider = GetComponent<Slider>();

		slider.value = PlayerPrefs.GetFloat(key, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetValue(){
		PlayerPrefs.SetFloat(key, slider.value);
	}
}
