﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEndPanel : MonoBehaviour {
	public GameObject panel;
	public Text text;
	public Text countdownText;

	void Start () {
		GameStateController.OnStateActivate += OnStateActivate;
		panel.SetActive(false);
	}
	
	void Update () {
		
	}

	void OnDestroy() {
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	private void OnStateActivate(GameStateController.GameStates state){
		if(state == GameStateController.GameStates.Done){
			if(GameController.IsAllPlayerDead() && GameController.IsAllEnemyDead()){
				text.text = "Draw";
				AudioHandler.CreateSound("SFX_Lose");
			}
			else if(GameController.IsAllPlayerDead()){
				text.text = "Mission Failed";
				AudioHandler.CreateSound("SFX_Lose");
			}
			else if(GameController.IsAllEnemyDead()){
				text.text = "Mission Completed";
				AudioHandler.CreateSound("SFX_Victory");
			}
			panel.SetActive(true);
			StartCoroutine(CountdawnToLobby());
		}
	}

	private IEnumerator CountdawnToLobby()
	{
		for (int i = 5; i > 0; i--)
		{
			countdownText.text = i.ToString();
			yield return new WaitForSeconds(1);
		}
		LevelManager.instance.ToLobby();
	}
}
