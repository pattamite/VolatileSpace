﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPanel : MonoBehaviour {

	public GameObject panel;

	void Start () {
		panel.SetActive(true);
		GameStateController.OnStateActivate += OnStateActivate;
	}

	void OnDestroy(){
		GameStateController.OnStateActivate -= OnStateActivate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnStateActivate(GameStateController.GameStates state){
		switch(state){
			case GameStateController.GameStates.Init :
				break;
			case GameStateController.GameStates.NetworkInit :
				break;
			default:
				panel.SetActive(false);
				break;
		}
	}
}
