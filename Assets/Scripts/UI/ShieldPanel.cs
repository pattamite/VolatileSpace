﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldPanel : MonoBehaviour {

	[Header("UI Objects")]
	public GameObject shieldPanel;
	public CircularSlider circularSlider;
	public GameObject model;
	public GameObject[] playerModel;
	public GameObject shieldVisual; 

	private Ship currentShip;

	void OnValidate() {
		if (playerModel.Length != 2) {
			System.Array.Resize(ref playerModel, 2);
		}
	}

	void Awake() {
		CheckComponent();
	}

	void Start() {
		ShipDeselected(null);
		playerModel[0].SetActive(false);
		playerModel[1].SetActive(false);

		PlayerController.OnShipSelected += ShipSelected;
		PlayerController.OnShipDeselected += ShipDeselected;
		GameStateController.OnStateActivate += OnStateActivate;
		Ship.OnDead += OnShipDead;
		circularSlider.OnUserInputValue += ValueChangeCheck;
	}
	void Update() {
		if (currentShip) {
			model.transform.eulerAngles = currentShip.transform.eulerAngles;
			circularSlider.transform.eulerAngles = currentShip.transform.eulerAngles;
		}
	}

	void OnDestroy() {
		PlayerController.OnShipSelected -= ShipSelected;
		PlayerController.OnShipDeselected -= ShipDeselected;
		GameStateController.OnStateActivate -= OnStateActivate;
		Ship.OnDead -= OnShipDead;
		circularSlider.OnUserInputValue -= ValueChangeCheck;
	}

	private void CheckComponent() {
		Debug.Assert(shieldPanel, gameObject.name + ": shieldPanel not found!");
		Debug.Assert(circularSlider, gameObject.name + ": slider not found!");
		Debug.Assert(model, gameObject.name + ": model not found!");
	}

	public void ShipSelected(Ship ship) {
		shieldPanel.SetActive(true);
		playerModel[ship.shipOwnerId - 1].SetActive(true);
		playerModel[2 - ship.shipOwnerId].SetActive(false);
		currentShip = ship;
		SetCurrentValue();
		SetSliderInteractable((GameController.IsPlayerId(ship.shipOwnerId) || GameController.isOfflineModeActivate) &&
			(GameStateController.currentGameState == GameStateController.GameStates.Planning));
	}

	public void ShipDeselected(Ship ship) {
		shieldPanel.SetActive(false);
	}

	private void SetCurrentValue() {
		if (currentShip) {
			circularSlider.SetValue(currentShip.GetShieldFocusAngle());
			shieldVisual.transform.localEulerAngles = new Vector3(0, 0, 
				currentShip.transform.eulerAngles.z + currentShip.GetShieldFocusAngle());
		}
	}

	public void ValueChangeCheck(float value) {
		if (currentShip.SetShieldFocusAngle(value)) {
			SetCurrentValue();
		}
		else {
			SetCurrentValue();
		}
	}

	public void SetSliderInteractable(bool value) {
		circularSlider.interactable = value;
	}

	private void OnStateActivate(GameStateController.GameStates state) {
		if (state == GameStateController.GameStates.Planning) {
			if (currentShip && (GameController.IsPlayerId(currentShip.shipOwnerId) ||
					GameController.isOfflineModeActivate)) {
				SetSliderInteractable(true);
			}
		}
		else {
			SetSliderInteractable(false);
		}
	}

	private void OnShipDead(Ship ship) {
		if (ship == currentShip) {
			shieldPanel.SetActive(false);
		}
	}

	public void SetPlayerModel(GameObject modelObject, int id){
		id--;
		if(id >= 0 && id <= 1){
			modelObject.transform.parent = model.transform;
			modelObject.transform.eulerAngles = playerModel[id].transform.eulerAngles;
			modelObject.transform.position = playerModel[id].transform.position;
			modelObject.transform.localScale = playerModel[id].transform.localScale;
			playerModel[id] = modelObject;

			ShipModel shipModel = modelObject.GetComponent<ShipModel>();
			if(shipModel){
				shipModel.DisableAllTrail();
			}
			modelObject.SetActive(false);
		}
	}
}