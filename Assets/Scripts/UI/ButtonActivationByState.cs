﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonActivationByState : MonoBehaviour {
	public GameStateController.GameStates[] statesToActivate;
	public GameStateController.GameStates targetState;

	private Button button;

	// Use this for initialization
	void Awake() {
		

	}

	void Start() {
		button = GetComponent<Button>();
		button.interactable = false;
		GameStateController.OnStateActivate += OnStateActivate;
	}

	// Update is called once per frame
	void Update() {

	}

	void OnDestroy() {
		GameStateController.OnStateActivate -= OnStateActivate;
	}

	private void OnStateActivate(GameStateController.GameStates state) {
		bool isInteractable = false;
		foreach (GameStateController.GameStates activateState in statesToActivate) {
			isInteractable |= (state == activateState);
		}

		button.interactable = isInteractable;
	}

	public void ForceActivateState() {
		GameStateController.ForceActivateState(targetState, true);
	}
}