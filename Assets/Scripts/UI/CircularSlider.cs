﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CircularSlider : MonoBehaviour, IDragHandler {

	public Canvas canvas;
	public RectTransform handle;
	public float radius = 100;
	public float value {get; private set;}
	public bool interactable = true;

	public delegate void ValueEvent(float value);
	public event ValueEvent OnUserInputValue;

	private Vector3 center;

	private void Start() {
		center = Camera.main.WorldToScreenPoint(this.GetComponent<RectTransform>().position);
		center.z = 0;
	}

	public void OnDrag(PointerEventData eventData) {
		if (interactable) {
			Vector3 ptrPos = eventData.position;
			Vector3 dir = (ptrPos - center).normalized;
			Vector3 handlePos = canvas != null ? center + dir * radius * canvas.scaleFactor : center + dir * radius;
			float newValue = Mod(((Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) - transform.eulerAngles.z), 360);

			if (newValue != value) {
				value = newValue;
				if (OnUserInputValue != null) OnUserInputValue(value);
			}

			SetPositionByValue();
		}

	}

	private void SetPositionByValue(){
		float worldAngle = value + transform.eulerAngles.z;
		Vector3 prevPos = Camera.main.WorldToScreenPoint(handle.position);
		Vector3 dir = new Vector3(Mathf.Cos(worldAngle * Mathf.Deg2Rad),
			Mathf.Sin(worldAngle * Mathf.Deg2Rad), 0);
		Vector3 handlePos = canvas != null ? center + dir * radius * canvas.scaleFactor : center + dir * radius;
		handlePos.Set(handlePos.x, handlePos.y, prevPos.z);
		handle.position = Camera.main.ScreenToWorldPoint(handlePos);
	}

	public void SetValue(float value) {
		this.value = value;
		SetPositionByValue();
	}

	private float Mod(float a, float b){
		return ((a % b) + b) % b;
	}
}
