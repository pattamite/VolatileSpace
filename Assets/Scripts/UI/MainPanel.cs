﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainPanel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler {

	void Awake() { }

	void Start() { }

	void Update() { }

	public void OnPointerClick(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftClick,
				eventData.position);
		}
	}

	public void OnDrag(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftDrag,
				eventData.position);
		}
	}

	public void OnPointerDown(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftDown,
				eventData.position);
		}
	}

	public void OnPointerUp(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftUp,
				eventData.position);
		}
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftBeginDrag,
				eventData.position);
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
		if (InputCondition(eventData)) {
			PlayerController.MainPanelUserInputHandling(PlayerController.InputTypes.LeftEndDrag,
				eventData.position);
		}
	}

	private bool InputCondition(PointerEventData eventData) {
		return eventData.button == PointerEventData.InputButton.Left ||
			(Input.touchSupported && Input.touchCount == 1);
	}
}