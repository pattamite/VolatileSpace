﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipSelectButton : MonoBehaviour {
	[Header("ID")]
	public int shipIndex;

	[Header("Options")]
	public bool moveCameraToShip;

	private Ship currentShip {
		get {
			return GameController.GetShipsByPlayerId(GameController.instance.playerId)[shipIndex];
		}
	}
	private Button button;

	// Use this for initialization
	void Start() {
		button = GetComponent<Button>();
		Ship.OnDead += CheckDead;
	}

	// Update is called once per frame
	void Update() {

	}

	void OnDestroy() {
		Ship.OnDead -= CheckDead;
	}

	private void CheckDead(Ship ship) {
		if (currentShip == ship) {
			button.interactable = false;
		}
	}

	public void SelectShip() {
		PlayerController.ForceSelectShip(currentShip);
		if(moveCameraToShip) CameraController.SetCameraPositionToShip(currentShip);
	}
}