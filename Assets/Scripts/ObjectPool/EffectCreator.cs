﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectCreator : MonoBehaviour
{

	public static EffectCreator instance {get;private set;}

	private void Awake()
	{
		instance = this;
	}

	public Bullet CreateWeaponFire(Vector3 from, Collider to, Bullet.Job onFinish = null)
	{
		GameObject bulletObj = ObjectPool.Instances["bullet"].GetOrCreate();
		bulletObj.SetActive(true);
		Bullet bullet = bulletObj.GetComponent<Bullet>();
		bullet.transform.position = from;
		bullet.Init(to);
		AudioHandler.CreateSound("SFX_ShipFire", bulletObj.transform.position);
		if (onFinish != null)
			bullet.OnFinished += onFinish;
		return bullet;
	}
	
	public void CreateShipExplode(Vector3 pos)
	{
		GameObject explosionObj =  ObjectPool.Instances["shipExplosion"].GetOrCreate();
		explosionObj.SetActive(true);
		explosionObj.transform.position = pos;
		explosionObj.transform.rotation = Quaternion.Euler(Vector3.zero);
		explosionObj.GetComponent<ParticleSystem>().time = 0;
		ObjectPool.Instances["shipExplosion"].Release(explosionObj, 3);
		AudioHandler.CreateSound("SFX_ShipDestroyed", explosionObj.transform.position);
	}
}
