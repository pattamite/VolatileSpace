﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Range(1f, 100f)]
    public float speed;

    public bool canExplode;
    public string explosionPoolName;
    public float explosionTime = 2;
    
    
    public delegate void Job();
    public event Job OnFinished;
    
    [HideInInspector]
    public Collider target;
    [HideInInspector]
    public BulletPool pool;

    void FixedUpdate()
    {
        Vector3 closetPoint = target.ClosestPointOnBounds(transform.position);
        Vector3 diff = closetPoint - transform.position;
        Vector3 dir = (target.transform.position - transform.position).normalized;
        Vector3 step = dir * Time.fixedDeltaTime * speed;
        transform.LookAt(target.transform);
        transform.position = transform.position + step;
        if (step.magnitude > diff.magnitude)
        {
            SelfDestroy(closetPoint);
        }
    }

    public void Init(Collider target)
    {
        this.target = target;
        transform.LookAt(target.transform);
    }

    private void SelfDestroy()
    {
        SelfDestroy(transform.position);
    }

    private void SelfDestroy(Vector3 position)
    {
        if (canExplode)
        {
            GameObject explosion = ObjectPool.Instances[explosionPoolName].GetOrCreate();
            explosion.transform.position = position;
            explosion.SetActive(true);
            explosion.GetComponent<ParticleSystem>().time = 0;
            ObjectPool.Instances[explosionPoolName].Release(explosion, explosionTime);
            //AudioHandler.CreateSound("SFX_ShipHit", transform.position);
        }

        if (OnFinished != null)
        {
            OnFinished();
            OnFinished = null;
        }
        pool.Release(gameObject);
    }
}
