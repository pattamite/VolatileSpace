﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

	public string name;
	public GameObject prefab;
	public int createOnStart = 5;
	
	private Queue<GameObject> reserved = new Queue<GameObject>();
	
	private static readonly Dictionary<string,ObjectPool> instances = new Dictionary<string, ObjectPool>();

	public static Dictionary<string,ObjectPool> Instances 
	{
		get { return instances; }
		private set { }
	}
	
	void Start ()
	{
		if (instances.ContainsKey(name))
			instances.Remove(name);
		instances.Add(name, this);
		for (int i = 0; i < createOnStart; i++)
		{
			reserved.Enqueue(CreateNew());
		}
	}

	void OnDestroy()
	{
		instances.Remove(name);
	}

	public GameObject GetOrCreate()
	{
		if (reserved.Count > 0)
		{
			return reserved.Dequeue();
		}
		else
		{
			return CreateNew();
		}
	}

	public void Release(GameObject o)
	{
		o.SetActive(false);
		reserved.Enqueue(o);
	}
	
	public void Release(GameObject o, float time)
	{
		StartCoroutine(ReleaseWithDelay(o, time));
	}

	private IEnumerator ReleaseWithDelay(GameObject o, float time)
	{
		yield return new WaitForSeconds(time);
		o.SetActive(false);
		reserved.Enqueue(o);
	}

	protected virtual GameObject CreateNew()
	{
		GameObject o = Instantiate(prefab, transform);
		o.SetActive(false);
		return o;
	}
}
