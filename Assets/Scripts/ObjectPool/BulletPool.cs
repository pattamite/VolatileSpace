﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : ObjectPool
{
	protected override GameObject CreateNew()
	{
		GameObject bObj =Instantiate(prefab, transform);
		bObj.SetActive(false);
		Bullet bullet = bObj.GetComponent<Bullet>();
		bullet.pool = this;
		return bObj;
	}
}
