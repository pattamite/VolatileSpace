﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class BackTekan : MonoBehaviour
{
    public void BackGame()
    {
        Destroy(NetworkLobbyManager.singleton.gameObject);
        SceneManager.LoadScene(0);
    }
}
