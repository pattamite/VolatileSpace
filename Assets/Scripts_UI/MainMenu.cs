﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void Start()
    {
        if (NetworkLobbyManager.singleton != null)
        {
            NetworkLobbyManager.Shutdown();
        }
    }
    
    public void PlayGame()
    {
        SceneManager.LoadScene("Lobby");
    }
    

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}
