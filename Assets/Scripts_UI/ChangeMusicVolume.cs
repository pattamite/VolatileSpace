﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMusicVolume : MonoBehaviour {

    public Slider Volume;
    public AudioSource BackgroundMusic;
    public AudioSource InGameMusic;

	// Update is called once per frame
	void Update ()
    {
        BackgroundMusic.volume = VolumeController.GetVolume(VolumeController.VolumeType.BGM);
	}
}
